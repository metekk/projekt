namespace Games.Api.DTO;

public class GamesPage
{
    private int _id;
    private string _name;
    private string _description;


    public int GameId {
        get{
            return _id;
        }
    } 
    public string GameDescription {
        get{
            return _description;
        }
    } 
    public string GameName {
        get{
            return _name;
        }
    } 

    public GamesPage(int id, string name, string description)
    {
        _id = id;
        _name = name;
        _description = description;
    }
}