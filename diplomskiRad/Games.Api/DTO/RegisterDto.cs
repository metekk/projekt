namespace Games.Api.DTO;

public class RegisterDto
{
    public string Name { set; get;}
    public string Email { set; get; }
    public string Password { set; get; }
    public string School { set; get; }
}

