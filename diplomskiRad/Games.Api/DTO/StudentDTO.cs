namespace Games.Api.DTO;
public class StudentDTO
{
    public int StudentID { get; set; }

    public string Initials { get; set; }


    public int Age { get; set; }


    public int TeacherID { get; set; }

    public int DifficultyType { get; set; }

    public int ComputerUse { get; set; }

    public int TouchscreenUse { get; set; }

    public int DegreeID { get; set; }

    public int MotorSkills { get; set; }

    public int StudentReading { get; set; }

    public int StudentWriting { get; set; }

    public int StudentMath { get; set; }

    public int StudentTime { get; set; }

    public int StudentMoney { get; set; }

    public int StudentCommunication { get; set; }

    public int StudentConversation { get; set; }

    public int StudentLanguageUnderstanding { get; set; }

    public int StudentLanguageProduction { get; set; }

    public int StudentEmotions { get; set; }
    public int StudentRules { get; set; }
    public int StudentChangingActivities { get; set; }
    public int StudentOtherPeople { get; set; }
    public int StudentRiskSocSit { get; set; }
    public int StudentManipulation { get; set; }
    public int StudentFeeding { get; set; }
    public int StudentDressing { get; set; }
    public int StudentHygiene { get; set; }
    public int StudentSchool { get; set; }
    public int StudentTransportation { get; set; }
    public int StudentWorkAct { get; set; }
    public int StudentFreeTime { get; set; }


}