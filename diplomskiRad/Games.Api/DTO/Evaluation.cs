namespace Games.Api.DTO;
public class Evaluation
{
    private int _id;
    private int _evaluationValue;

    public int GameId {
        get{
            return _id;
        }
        set{
            _id = value;
        }
    } 
    public int GameEvaluation {
        get{
            return _evaluationValue;
        }
        set {
            _evaluationValue = value;
        }
    }
}