namespace Games.Api.DTO;
public class CommentDTO
{
    public int _id { get; set; }
   
    public int GameId { get; set; }

    public int TeacherId { get; set; }

    public string GameComment { get; set; }

    public int Evaluation { get; set; }
}