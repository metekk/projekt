namespace Games.Api.DTO;
public class Visualization
{
    private int _id;
    private string _name;
    private int _evaluationValue;

    public int GameId {
        get{
            return _id;
        }
    }
    public string GameName {
        get{
            return _name;
        }
        set{
            _name = value;
        }
    } 
    public int GameEvaluation {
        get{
            return _evaluationValue;
        }
        set{
            _evaluationValue = value;
        }
    }

    public Visualization(int id, string name, int evaluation)
    {
        _id = id;
        _name = name;
        _evaluationValue = evaluation;
    }
}