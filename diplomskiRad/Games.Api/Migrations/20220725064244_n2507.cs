﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Games.Api.Migrations
{
    public partial class n2507 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    StudentID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Initials = table.Column<string>(type: "TEXT", nullable: true),
                    Age = table.Column<int>(type: "INTEGER", nullable: false),
                    TeacherID = table.Column<int>(type: "INTEGER", nullable: false),
                    DifficultyType = table.Column<int>(type: "INTEGER", nullable: false),
                    ComputerUse = table.Column<int>(type: "INTEGER", nullable: false),
                    TouchscreenUse = table.Column<int>(type: "INTEGER", nullable: false),
                    DegreeID = table.Column<int>(type: "INTEGER", nullable: false),
                    MotorSkills = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentReading = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentWriting = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentMath = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentTime = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentMoney = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentCommunication = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentConversation = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentLanguageUnderstanding = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentLanguageProduction = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentEmotions = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentRules = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentChangingActivities = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentOtherPeople = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentRiskSocSit = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentManipulation = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentFeeding = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentDressing = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentHygiene = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentSchool = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentTransportation = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentWorkAct = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentFreeTime = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.StudentID);
                    table.ForeignKey(
                        name: "FK_Students_Teachers_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });*/

            migrationBuilder.CreateIndex(
                name: "IX_Students_TeacherID",
                table: "Students",
                column: "TeacherID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Students");
        }
    }
}
