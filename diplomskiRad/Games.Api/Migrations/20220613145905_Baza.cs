﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Games.Api.Migrations
{
    public partial class Baza : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Games",
                columns: table => new
                {
                    GameId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    GamePlatform = table.Column<string>(type: "TEXT", nullable: true),
                    GameDescription = table.Column<string>(type: "TEXT", nullable: true),
                    GameField = table.Column<string>(type: "TEXT", nullable: true),
                    GameName = table.Column<string>(type: "TEXT", nullable: true),
                    GameLink = table.Column<string>(type: "TEXT", nullable: true),
                    GameFilePath = table.Column<string>(type: "TEXT", nullable: true),
                    GameVideoPath = table.Column<string>(type: "TEXT", nullable: true),
                    GameInstructions = table.Column<string>(type: "TEXT", nullable: true),
                    GameRules = table.Column<string>(type: "TEXT", nullable: true),
                    GameDuration = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Games", x => x.GameId);
                });

            migrationBuilder.CreateTable(
                name: "Teachers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Email = table.Column<string>(type: "TEXT", nullable: true),
                    Password = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teachers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CommentGames",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    GameId = table.Column<int>(type: "INTEGER", nullable: false),
                    TeacherId = table.Column<int>(type: "INTEGER", nullable: false),
                    Comment = table.Column<string>(type: "TEXT", nullable: true),
                    Evaluation = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommentGames", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommentGames_Games_GameID",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "GameID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CommentGames_GameId",
                table: "CommentGames",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "IX_Teachers_Email",
                table: "Teachers",
                column: "Email",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommentGames");

            migrationBuilder.DropTable(
                name: "Teachers");

            migrationBuilder.DropTable(
                name: "Games");
        }
    }
}
