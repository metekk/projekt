using Microsoft.EntityFrameworkCore;
using Games.Infrastructure;
using Games.Api.Helpers;

using JsonPatchSample;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

/*builder.Services.AddControllers(options =>
{
    options.InputFormatters.Insert(0, MyJPIF.GetJsonPatchInputFormatter());
});*/

//builder.Services.AddControllers()
//    .AddNewtonsoftJson();


    builder.Services.AddControllers().AddNewtonsoftJson();
    builder.Services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "ShoppingCart", Version = "v1" });
        c.CustomSchemaIds(x => x.FullName);
        c.EnableAnnotations();
        c.DocumentFilter<JsonPatchDocumentFilter>();
    });



builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<JwtService>();
// Add database context
builder.Services.AddDbContext<GamesContext>(options =>
{
    options.UseSqlite("Data Source=games.db", b => b.MigrationsAssembly("Games.Api"));
});
builder.Services.AddCoreAdmin();
var app = builder.Build();


// Configure the HTTP request pipeline.



if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseHttpsRedirection();
app.UseRouting();
//app.UseHttpsRedirection();
app.UseCors(options => options
    .WithOrigins(new []{"http://localhost:3000", "http://localhost:8000", "http://localhost:4200"})
    .AllowAnyHeader()
    .AllowAnyMethod()
    .AllowCredentials()
);

app.UseAuthorization();

app.MapControllers();
app.MapDefaultControllerRoute();
app.Run();