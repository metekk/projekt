using DiplomskiClassLib;
using Microsoft.AspNetCore.Mvc;
using Games.Infrastructure;
using Games.Api.DTO;
using Games.Api.Helpers;
namespace Games.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthController : Controller
    {        
        private readonly IUserRepository _repository;
        private readonly JwtService _jwtService;
        public AuthController(IUserRepository repository, JwtService jwtService){
            _repository = repository;
            _jwtService = jwtService;
        }

        [HttpPost("register")]
        public IActionResult Register(RegisterDto dto)
        {
            var teacher = new Teacher 
            {
                Name = dto.Name,
                Email = dto.Email,
                Password = BCrypt.Net.BCrypt.HashPassword(dto.Password),
                School = dto.School
            };
             
            return Created("ok", _repository.Create(teacher));
        }

        [HttpPost("login")]
        public IActionResult Login(LoginDto dto)
        {
            var teacher = _repository.GetByEmail(dto.Email);

            if (teacher == null){
                return BadRequest(new{message = "Invalid credentials"});
            } 

            if (!BCrypt.Net.BCrypt.Verify(dto.Password, teacher.Password)){
                return BadRequest(new{message = "Invalid credentials"});
            }

            if (teacher.Email != dto.Email){
                return BadRequest(new{message = "Invalid credentials"});
            }

            var jwt = _jwtService.Generate(teacher.Id);
            // Console.WriteLine(teacher.Id);

            //for frontend to get it, not see it
            Response.Cookies.Append("jwt", jwt, new CookieOptions
            {
                HttpOnly = true
            });

            return Ok(new
                {
                    message = "success",
                    email = teacher.Email,
                    password = teacher.Password,
                    teacherId = teacher.Id
                });
        }

        [HttpGet("teacher")]
        public IActionResult Teacher(){

            try{
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);
                int teacherId = int.Parse(token.Issuer);
                var teacher = _repository.GetById(teacherId);

                return Ok(teacher);
            }catch(Exception ){
                return Unauthorized();
            }
        }

        [HttpPost("Logout")]
        public IActionResult Logout()
        {
            Response.Cookies.Delete("jwt");
            return Ok(new
            {
                message = "success"
            });
        }
    }   
}