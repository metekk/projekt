using DiplomskiClassLib;
using Games.Api.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Games.Infrastructure;
namespace Games.Api.Controllers;
using System.Linq;  
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

[ApiController]
[Route("[controller]")]
public class CommentController : ControllerBase
{
    private GamesContext _context;
    public CommentController(GamesContext gamesContext)
    {
        _context = gamesContext;
    }

    /*
    // Typed lambda expression for Select() method. 
        private static readonly Expression<Func<CommentGame, CommentDTO>> AsCommentDto =
            newComment => new CommentDTO
            {
                _id = newComment.Id,
                GameId = newComment.GameId,
                TeacherId = newComment.TeacherId,
                GameComment = newComment.Comment,
                Evaluation = newComment.Evaluation
            };

    [Route("comment-for-game")]
    [HttpGet]
    public IQueryable<CommentDTO> GetCommentForGame (int gameId) {
        return _context.CommentGames.Include(b => b.Game)
            .Where(b => b.GameId == gameId)
            .Select(AsCommentDto);
    }*/


    [HttpGet("games-comment/{id}")]
    public List<CommentGame> GetComment(int id)
    {
        return _context.CommentGames.OfType<CommentGame>().Where(b => b.GameId == id).ToList();
        //return _context.CommentGames.FirstOrDefault(b => b.GameId == id);
    }
    
    [HttpPost("new-comment")]
    public IActionResult NewComment([FromBody] CommentGame newComment)
    {
        // comment game
        CommentGame app = new(       
            newComment.GameId,
            newComment.TeacherId,
            newComment.Evaluation,
            newComment.Comment);

        _context.CommentGames.Add(app);
        _context.SaveChanges();
        return Ok();
    }
}