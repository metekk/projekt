using DiplomskiClassLib;
using Games.Api.DTO;
using Games.Infrastructure;
namespace Games.Api.Controllers;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Linq;  
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System;  
using System.Collections.Generic;   
using System.Net;  
using System.Net.Http;

[ApiController]
[Route("[controller]")]


public class StudentController : ControllerBase
{
    private GamesContext _context;
    public StudentController(GamesContext gamesContext)
    {
        _context = gamesContext;
    }

    /*
        // Typed lambda expression for Select() method. 
        private static readonly Expression<Func<Student, StudentDTO>> AsStudentDto =
            newStudent => new StudentDTO
            {
            StudentID = newStudent.StudentID,
            Initials = newStudent.Initials,
            Age = newStudent.Age,
           TeacherID =newStudent.TeacherID,
          DifficultyType  =newStudent.DifficultyType,
          ComputerUse  =newStudent.ComputerUse,
           TouchscreenUse =newStudent.TouchscreenUse,
           DegreeID =newStudent.DegreeID,
           MotorSkills =newStudent.MotorSkills,
           StudentReading =newStudent.StudentReading,
           StudentWriting =newStudent.StudentWriting,
           StudentMath =newStudent.StudentMath,
           StudentTime =newStudent.StudentTime,
            StudentMoney=newStudent.StudentMoney,
            StudentCommunication=newStudent.StudentCommunication,
           StudentConversation =newStudent.StudentConversation,
           StudentLanguageUnderstanding =newStudent.StudentLanguageUnderstanding,
           StudentLanguageProduction =newStudent.StudentLanguageProduction,
           StudentEmotions =newStudent.StudentEmotions,
           StudentRules =newStudent.StudentRules,
           StudentChangingActivities =newStudent.StudentChangingActivities,
           StudentOtherPeople =newStudent.StudentOtherPeople,
            StudentRiskSocSit=newStudent.StudentRiskSocSit,
            StudentManipulation=newStudent.StudentManipulation,
            StudentFeeding=newStudent.StudentFeeding,
            StudentDressing=newStudent.StudentDressing,
           StudentHygiene =newStudent.StudentHygiene,
            StudentSchool=newStudent.StudentSchool,
           StudentTransportation =newStudent.StudentTransportation,
            StudentWorkAct=newStudent.StudentWorkAct,
           StudentFreeTime =newStudent.StudentFreeTime
            };*/


    /*[HttpGet("student/{id}")]
    public List<Student> GetStudent(int id)
    {
        return _context.Students.OfType<Student>().Where(b => b.StudentID == id).ToList();

    }*/

    [Route("Studentdetails")]  
        [HttpGet]  
        public object   Studentdetails()  
        {  
             
                var a = _context.Students.ToList();  
                return a;  
        } 
     
    /*
    [Route("student-by-teacher")]
    [HttpGet]
    public IQueryable<StudentDTO> GetStudentByTeacher(int teacherId) {
        return _context.Students.Include(b => b.Teacher)
            .Where(b => b.TeacherID == teacherId)
            .Select(AsStudentDto);
    }*/

    [HttpGet("student-by-teacher/{id}")]
    public List<Student> GetStudent(int id)
    {
        return _context.Students.OfType<Student>().Where(b => b.TeacherID == id).ToList();
    }

     /*[HttpGet("student-by-id")]
    public List<Student> GetStudentId(int id)
    {
        return _context.Students.OfType<Student>().Where(b => b.StudentID == id).ToList();
    }*/


    [Route("StudentdetailById/{id}")]  
    [HttpGet]  
    public object StudentdetailById(int id)  {  
            var obj = _context.Students.Where(x => x.StudentID == id).ToList().FirstOrDefault();  
            return obj;  
        } 
    
    [HttpGet("student-detail/{id}")]
    public Student GetStudent2(int id)
    {
        // list of students
        return _context.Students.Single(b => b.StudentID == id);
    }



    [Route("new-student")]  
    [HttpPost]  
    public IActionResult NewStudent([FromBody] Student newStudent)
    {
        // student
        Student app = new(
            newStudent.StudentID,
            newStudent.Initials,
            newStudent.Age,
            newStudent.TeacherID,
            newStudent.DifficultyType,
            newStudent.ComputerUse,
            newStudent.TouchscreenUse,
            newStudent.DegreeID,
            newStudent.MotorSkills,
            newStudent.StudentReading,
            newStudent.StudentWriting,
            newStudent.StudentMath,
            newStudent.StudentTime,
            newStudent.StudentMoney,
            newStudent.StudentCommunication,
            newStudent.StudentConversation,
            newStudent.StudentLanguageUnderstanding,
            newStudent.StudentLanguageProduction,
            newStudent.StudentEmotions,
            newStudent.StudentRules,
            newStudent.StudentChangingActivities,
            newStudent.StudentOtherPeople,
            newStudent.StudentRiskSocSit,
            newStudent.StudentManipulation,
            newStudent.StudentFeeding,
            newStudent.StudentDressing,
            newStudent.StudentHygiene,
            newStudent.StudentSchool,
            newStudent.StudentTransportation,
            newStudent.StudentWorkAct,
            newStudent.StudentFreeTime);
    	
        _context.Students.Add(app);
        _context.SaveChanges();
        return Ok("Success");
    }




    [Route("update-student/{id}")]
    [HttpPut]
    public IActionResult UpdateStudent(int id, [FromBody] Student student) {
        var pached = _context.Students.AsNoTracking().FirstOrDefault(b => b.StudentID == id);
        student.TeacherID = pached.TeacherID;
        _context.Update(student);
        _context.SaveChanges();
        var model = new {
            sent = student,
            after = pached
        };
        return Ok(model);
    }


    /*[HttpPatch("{studentId2}")]
    public IActionResult Patch2(int id, [FromBody] JsonPatchDocument<Student> patch)
    {
        var fromDb = _context.Students.FirstOrDefault(b => b.StudentID == id);

        var original = fromDb.Copy();
        patch.ApplyTo(fromDb, ModelState);

        var isValid = TryValidateModel(fromDb);
        if (!isValid)
        {
            return BadRequest(ModelState);
        }
        _context.Update(fromDb);
        _context.SaveChanges(); var model = new
        {
            original,
            patched = fromDb
        }; return Ok(model);
    }*/


    private bool StudentExists(int id)
    {
        return _context.Students.Any(e => e.StudentID == id);
    }


    [HttpPatch("EditStudent/{id:int}")]
    public IActionResult Patch(int id, [FromBody] JsonPatchDocument<Student> patchEntity)
    {
        var entity = _context.Students.FirstOrDefault(b => b.StudentID == id);

        if (entity == null)
        {
            return NotFound();
        }

        patchEntity.ApplyTo(entity, ModelState); // Must have Microsoft.AspNetCore.Mvc.NewtonsoftJson installed

        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        _context.Update(entity); //Update in the database.

        try
        {
            _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!StudentExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }
        return Ok("Updated");
    }

    /*[HttpDelete("delete-student")]
    public IActionResult Delete(int id)
    {
        var Student = _context.Students.Find(id);
        _context.Students.Remove(Student);
        _context.SaveChanges();

        return Ok("Delete"); 
    }*/

    [Route("Deletestudent")]  
        [HttpDelete]  
        public object Deletestudent(int id)  
        {  
            var obj = _context.Students.Where(x => x.StudentID == id).ToList().FirstOrDefault();  
            _context.Students.Remove(obj);  
            _context.SaveChanges();  
            return new Response  {  
              Status = "Delete",  
              Message = "Delete Successfuly"  
          }; 
        }  



}
