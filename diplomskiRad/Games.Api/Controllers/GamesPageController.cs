using DiplomskiClassLib;
using Games.Api.DTO;
using Games.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace Games.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class GamesPageController : ControllerBase
{
    private GamesContext _context;
    public GamesPageController(GamesContext gamesContext)
    {
        _context = gamesContext;
    }
    [HttpGet("games-page")]
    public List<Game> GetAllGames()
    {
        //query returns id, name, desc 
        var results = _context.Games.Select(g => new Game 
        {
            GameID=g.GameID,
            GameName=g.GameName,
            Description=g.Description,
            Platform=g.Platform,
            FilePath=g.FilePath,
            URLiOS=g.URLiOS,
            URLAndroid=g.URLAndroid,
            URLweb=g.URLweb,
            Audio=g.Audio,
            Instructions=g.Instructions,
            Rules=g.Rules,
            Duration=g.Duration,
            Feedback=g.Feedback,
            MentalProcess=g.MentalProcess,
            KnowledgeDomain=g.KnowledgeDomain,
            Field=g.Field,
            GameReading=g.GameReading,
            GameWriting=g.GameWriting,
            GameMath=g.GameMath,
            GameTime=g.GameTime,
            GameMoney=g.GameMoney,
            GameCommunication=g.GameCommunication,
            GameConversation=g.GameConversation,
            GameLanguageUnderstanding=g.GameLanguageUnderstanding,
            GameLanguageProduction=g.GameLanguageProduction,
            GameEmotions=g.GameEmotions,
            GameRules=g.GameRules,
            GameChangingActivities=g.GameChangingActivities,
            GameOtherPeople=g.GameOtherPeople,
            GameRiskSocSit=g.GameRiskSocSit,
            GameManipulation=g.GameManipulation,
            GameFeeding=g.GameFeeding,
            GameDressing=g.GameDressing,
            GameHygiene=g.GameHygiene,
            GameSchool=g.GameSchool,    
            GameTransportation=g.GameTransportation,
            GameFreeTime=g.GameFreeTime,    
            GameWorkAct=g.GameWorkAct
        })
        .ToList();
        
        return results;
    }

    [HttpGet("games/{id}")]
    public List<Game> GetGames(int id)
    {
        var student = _context.Students.Where(x => x.StudentID == id).ToList().FirstOrDefault();
        //query returns id, name, desc
        var results = new List<Game>();
        if (student.StudentCommunication < 2){
            if ((student.DegreeID == 3 || student.DegreeID == 4) && (student.ComputerUse == 0 || student.TouchscreenUse == 0)){
                return null;
            }
            else if(student.StudentReading == 0){
                results = _context.Games.Where(x => (x.Field == 1) && (x.GameReading == 1) && (x.GameCommunication < 2)).ToList();
            }
            else if(student.StudentReading == 1){
                results = _context.Games.Where(x => (x.Field == 1) && (x.GameReading == 1 || x.GameReading == 2) && (x.GameCommunication < 2)).ToList();
            }
            else if(student.StudentReading == 2 || student.StudentReading == 21 || student.StudentReading == 22){
                results = _context.Games.Where(x => (x.Field == 1) && (x.GameReading == 2 || x.GameReading == 3) && (x.GameCommunication < 2)).ToList();
            }
            else if(student.StudentReading == 3 || student.StudentReading == 4){
                results = _context.Games.Where(x => (x.Field == 1) && (x.GameReading == 3 || x.GameReading == 4) && (x.GameCommunication < 2)).ToList();
            }
        }
        else{
            if ((student.DegreeID == 3 || student.DegreeID == 4) && (student.ComputerUse == 0 || student.TouchscreenUse == 0)){
                return null;
            }
            else if(student.StudentReading == 0){
                results = _context.Games.Where(x => (x.Field == 1) && (x.GameReading == 1)).ToList();
            }
            else if(student.StudentReading == 1){
                results = _context.Games.Where(x => (x.Field == 1) && (x.GameReading == 1 || x.GameReading == 2)).ToList();
            }
            else if(student.StudentReading == 2 || student.StudentReading == 21 || student.StudentReading == 22){
                results = _context.Games.Where(x => (x.Field == 1) && (x.GameReading == 2 || x.GameReading == 3)).ToList();
            }
            else if(student.StudentReading == 3 || student.StudentReading == 4){
                results = _context.Games.Where(x => (x.Field == 1) && (x.GameReading == 3 || x.GameReading == 4)).ToList();
            }
        }
        return results;
    }
}