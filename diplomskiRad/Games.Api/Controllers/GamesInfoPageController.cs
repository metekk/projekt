using DiplomskiClassLib;
using Games.Api.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Games.Infrastructure;
using Microsoft.EntityFrameworkCore;
namespace Games.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class GamesInfoPageController : ControllerBase
{
    private GamesContext _context;
    public GamesInfoPageController(GamesContext gamesContext)
    {
        _context = gamesContext;
    }

    [HttpGet("games-info/{id}")]
    public Game GetGame(int id)
    {
        // list of games with name, desc, picture
        return _context.Games.Include(g => g.Comments).Single(b => b.GameID == id);
    }

    [HttpPost("new-game")]
    public ActionResult NewGame([FromBody] Game newGame)
    {
        // save new game
        Game app = Game.Submit(
            newGame.GameID,
            newGame.GameName,
            newGame.Description,
            newGame.Platform,
            newGame.FilePath,
            newGame.URLiOS,
            newGame.URLAndroid,
            newGame.URLweb,
            newGame.Audio,
            newGame.Instructions,
            newGame.Rules,
            newGame.Duration,
            newGame.Feedback,
            newGame.MentalProcess,
            newGame.KnowledgeDomain,
            newGame.Field,
            newGame.GameReading,
            newGame.GameWriting,
            newGame.GameMath,
            newGame.GameTime,
            newGame.GameMoney,
            newGame.GameCommunication,
            newGame.GameConversation,
            newGame.GameLanguageUnderstanding,
            newGame.GameLanguageProduction,
            newGame.GameEmotions,
            newGame.GameRules,
            newGame.GameChangingActivities,
            newGame.GameOtherPeople,
            newGame.GameRiskSocSit,
            newGame.GameManipulation,
            newGame.GameFeeding,
            newGame.GameDressing,
            newGame.GameHygiene,
            newGame.GameSchool,
            newGame.GameTransportation,
            newGame.GameFreeTime,
            newGame.GameWorkAct );
        
        _context.Games.Add(app);
        _context.SaveChanges();

        return Ok();
    }

    [HttpDelete("delete")]
    public IActionResult Delete(int GameID)
    {
        var Game = _context.Games.Find(GameID);
        _context.Games.Remove(Game);
        _context.SaveChanges();

        return Ok();
    }

}