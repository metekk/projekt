# Functions:

1. New user has to register by typing in: 
    1. Name
    2. Surname
    3. e-mail
    4. Username
    5. Password
-> **signUp()**  
after registration user has to login
1. Already registered user has to login by entering:
    1. Username
    2. Password  
for using the app -> **login()**
1. After login/sign up user can see a list of games and their info -> **showGameInfo()**
    1. Name
    2. Description
    3. Age
    4. Field
    5. Link
    6. Evaluation
    7. Comment
1. User can choose which game wants to play -> **chooseGame()** 
1. User can see how previous users have rated and commented -> **showEvaluations()**
1. User can give evaluation and comment of each game -> **evaluateGame()**
1. User can see visualization of ratings for games -> **visualizeEvaluation()**
1. Administrator can enter new games and their data -> **enterGames()**

### Possible errors:

1. Entered wrong username or password
1. This username already exists ....
1. Not possible to retrieve data from the database
1. There are no evaluations or comments available (for the first user)
1. There is no visualization available

