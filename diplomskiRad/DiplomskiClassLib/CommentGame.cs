using System.ComponentModel.DataAnnotations.Schema;
namespace DiplomskiClassLib;
public class CommentGame
{

    private int _id;
    private int _evaluation;
    private string _comment;
    public int Id {
        get{
            return _id;
        }
        set{
            _id = value;
        }
    }
    public int GameId {get;set;}
    public int TeacherId { get; set;}
    
    public string Comment{
        get{
            return _comment;
        }
        set{
            _comment = value;
        }
    }

    public int Evaluation{
        get{
            return _evaluation;
        }
        set{
            _evaluation = value;
        }
    }

    //public Game Game { get; set; }
 
    
    public CommentGame(int gameId, int teacherId, int evaluation, string comment)
    {
        GameId = gameId;
        TeacherId = teacherId;
        Evaluation = evaluation;
        Comment = comment;
    }

    public CommentGame()
    {
        
    }
}