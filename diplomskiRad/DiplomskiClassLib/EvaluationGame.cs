namespace DiplomskiClassLib;
public class EvaluationGame
{
    private int _id;
    private int _evaluation;

    public int GameId {
        get{
            return _id;
        }
        set{
            _id = value;
        }
    } 
    public int GameEvaluation {
        get{
            return _evaluation;
        }
        set {
            _evaluation = value;
        }
    }

    public EvaluationGame(int id, int evaluation)
    {
        _id = id;
        _evaluation = evaluation;
    }
    public EvaluationGame(){
        
    }
}