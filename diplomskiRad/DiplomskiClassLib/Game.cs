using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;
namespace DiplomskiClassLib{

    public class Game {
        private int _gameID;
        private string _gameName;
        private string _description;
        private string _platform;
        private string _filePath;
        private string _URLiOS;
        private string _URLAndroid;
        private string _URLweb;
        private string _audio;
        private string _instructions;
        private string _rules;
        private int _duration;
        private int _feedback;
        private int _mentalProcess;
        private int _knowledgeDomain;
        private int _field;
        private int _gameReading;
        private int _gameWriting;
        private int _gameMath;
        private int _gameTime;
        private int _gameMoney;
        private int _gameCommunication;
        private int _gameConversation;
        private int _gameLanguageUnderstanding;
        private int _gameLanguageProduction;
        private int _gameEmotions;
        private string _gameRules;
        private string _gameChangingActivities;
        private string _gameOtherPeople;
        private string _gameRiskSocSit;
        private string _gameManipulation;
        private string _gameFeeding;
        private string _gameDressing;
        private string _gameHygiene;    
        private string _gameSchool;
        private string _gameTransportation;    
        private string _gameFreeTime;
        private string _gameWorkAct;   

    public static Game Submit(
          int gameID,
          string gameName,
          string description,
          string platform,
          string filePath,
          string URLiOS,
          string URLAndroid,
          string URLweb,
          string audio,
          string instructions,
          string rules,
          int duration,
          int feedback,
          int mentalProcess,
          int knowledgeDomain,
          int field,
          int gameReading,
          int gameWriting,
          int gameMath,
          int gameTime,
          int gameMoney,
          int gameCommunication,
          int gameConversation,
          int gameLanguageUnderstanding,
          int gameLanguageProduction,
          int gameEmotions,
          string gameRules,
          string gameChangingActivities,
          string gameOtherPeople,
          string gameRiskSocSit,
          string gameManipulation,
          string gameFeeding,
          string gameDressing,
          string gameHygiene,    
          string gameSchool,
          string gameTransportation,    
          string gameFreeTime,
          string gameWorkAct)

    {
        Game app = new();
            app.GameID=gameID;
            app.GameName=gameName;
            app.Description=description;
            app.Platform=platform;
            app.FilePath=filePath;
            app.URLiOS=URLiOS;
            app.URLAndroid=URLAndroid;
            app.URLweb=URLweb;
            app.Audio=audio;
            app.Instructions=instructions;
            app.Rules=rules;
            app.Duration=duration;
            app.Feedback=feedback;
            app.MentalProcess=mentalProcess;
            app.KnowledgeDomain=knowledgeDomain;
            app.Field=field;
            app.GameReading=gameReading;
            app.GameWriting=gameWriting;
            app.GameMath=gameMath;
            app.GameTime=gameTime;
            app.GameMoney=gameMoney;
            app.GameCommunication=gameCommunication;
            app.GameConversation=gameConversation;
            app.GameLanguageUnderstanding=gameLanguageUnderstanding;
            app.GameLanguageProduction=gameLanguageProduction;
            app.GameEmotions=gameEmotions;
            app.GameRules=gameRules;
            app.GameChangingActivities=gameChangingActivities;
            app.GameOtherPeople=gameOtherPeople;
            app.GameRiskSocSit=gameRiskSocSit;
            app.GameManipulation=gameManipulation;
            app.GameFeeding=gameFeeding;
            app.GameDressing=gameDressing;
            app.GameHygiene=gameHygiene;
            app.GameSchool=gameSchool;    
            app.GameTransportation=gameTransportation;
            app.GameFreeTime=gameFreeTime;    
            app.GameWorkAct=gameWorkAct;
            return app;
    }

    public int GameID {
        get{ return _gameID;}
        set{ _gameID = value;}
    } 

    public string GameName{
        get{ return _gameName;}
        set{ _gameName = value;}
    }

    public string Description{
            get{ return _description;}
            set{ _description = value;}
        }

    public string Platform{
            get{ return _platform;}
            set{ _platform = value;}
        }

    public string FilePath{
            get{ return _filePath;}
            set{ _filePath = value;}
        }

    public string URLiOS{
            get{ return _URLiOS;}
            set{ _URLiOS = value;}
        }

    public string URLAndroid{
            get{ return _URLAndroid;}
            set{ _URLAndroid = value;}
        }

    public string URLweb{
            get{ return _URLweb;}
            set{ _URLweb = value;}
        }

    public string Audio{
            get{ return _audio;}
            set{ _audio = value;}
        }

    public string Instructions{
            get{ return _instructions;}
            set{ _instructions = value;}
        }

    public string Rules{
            get{ return _rules;}
            set{ _rules = value;}
        }

    public int Duration{
            get{ return _duration;}
            set{ _duration = value;}
        }

    public int Feedback{
            get{ return _feedback;}
            set{ _feedback = value;}
        }

    public int MentalProcess{
            get{ return _mentalProcess;}
            set{ _mentalProcess = value;}
        }

    public int KnowledgeDomain{
            get{ return _knowledgeDomain;}
            set{ _knowledgeDomain = value;}
        }

    public int Field{
            get{ return _field;}
            set{ _field = value;}
        }

    public int GameReading{
            get{ return _gameReading;}
            set{ _gameReading = value;}
        }

    public int GameWriting{
            get{ return _gameWriting;}
            set{ _gameWriting = value;}
        }

    public int GameMath{
            get{ return _gameMath;}
            set{ _gameMath = value;}
        }

    public int GameTime{
            get{ return _gameTime;}
            set{ _gameTime = value;}
        }

    public int GameMoney{
            get{ return _gameMoney;}
            set{ _gameMoney = value;}
        }

    public int GameCommunication{
            get{ return _gameCommunication;}
            set{ _gameCommunication = value;}
        }

    public int GameConversation{
            get{ return _gameConversation;}
            set{ _gameConversation = value;}
        }

    public int GameLanguageUnderstanding{
            get{ return _gameLanguageUnderstanding;}
            set{ _gameLanguageUnderstanding = value;}
        }

    public int GameLanguageProduction{
            get{ return _gameLanguageProduction;}
            set{ _gameLanguageProduction = value;}
        }

    public int GameEmotions{
            get{ return _gameEmotions;}
            set{ _gameEmotions = value;}
        }

    public string GameRules{
            get{ return _gameRules;}
            set{ _gameRules = value;}
        }

    public string GameChangingActivities{
            get{ return _gameChangingActivities;}
            set{ _gameChangingActivities = value;}
        }

    public string GameOtherPeople{
            get{ return _gameOtherPeople;}
            set{ _gameOtherPeople = value;}
        }

    public string GameRiskSocSit{
            get{ return _gameRiskSocSit;}
            set{ _gameRiskSocSit = value;}
        }

    public string GameManipulation{
            get{ return _gameManipulation;}
            set{ _gameManipulation = value;}
        }
  
    public string GameFeeding{
            get{ return _gameFeeding;}
            set{ _gameFeeding = value;}
        }

    public string GameDressing{
            get{ return _gameDressing;}
            set{ _gameDressing = value;}
        }
    public string GameHygiene{
            get{ return _gameHygiene;}
            set{ _gameHygiene = value;}
        }

    public string GameSchool{
            get{ return _gameSchool;}
            set{ _gameSchool = value;}
        }
    public string GameTransportation{
            get{ return _gameTransportation;}
            set{ _gameTransportation = value;}
        }

    public string GameFreeTime{
            get{ return _gameFreeTime;}
            set{ _gameFreeTime = value;}
        }
    public string GameWorkAct{
            get{ return _gameWorkAct;}
            set{ _gameWorkAct = value;}
        }


    public List<CommentGame> Comments {get;set;}

    public Game(int gameID,string gameName,string description,string platform,string filePath,string URLiOS,string URLAndroid,string URLweb,string audio,string instructions,string rules,int duration,int feedback,int mentalProcess,int knowledgeDomain,int field,int gameReading,int gameWriting,int gameMath,int gameTime,int gameMoney,int gameCommunication,int gameConversation,int gameLanguageUnderstanding,int gameLanguageProduction,int gameEmotions,string gameRules,string gameChangingActivities,string gameOtherPeople,string gameRiskSocSit,string gameManipulation,string gameFeeding,string gameDressing,string gameHygiene,string gameSchool,string gameTransportation,string gameFreeTime,string gameWorkAct)
    {
            _gameID = gameID;
            _gameName=gameName;
            _description=description;
            _platform=platform;
            _filePath=filePath;
            _URLiOS=URLiOS;
            _URLAndroid=URLAndroid;
            _URLweb=URLweb;
            _audio=audio;
            _instructions=instructions;
            _rules=rules;
            _duration=duration;
            _feedback=feedback;
            _mentalProcess=mentalProcess;
            _knowledgeDomain=knowledgeDomain;
            _field=field;
            _gameReading=gameReading;
            _gameWriting=gameWriting;
            _gameMath=gameMath;
            _gameTime=gameTime;
            _gameMoney=gameMoney;
            _gameCommunication=gameCommunication;
            _gameConversation=gameConversation;
            _gameLanguageUnderstanding=gameLanguageUnderstanding;
            _gameLanguageProduction=gameLanguageProduction;
            _gameEmotions=gameEmotions;
            _gameRules=gameRules;
            _gameChangingActivities=gameChangingActivities;
            _gameOtherPeople=gameOtherPeople;
            _gameRiskSocSit=gameRiskSocSit;
            _gameManipulation=gameManipulation;
            _gameFeeding=gameFeeding;
            _gameDressing=gameDressing;
            _gameHygiene=gameHygiene;    
            _gameSchool=gameSchool;
            _gameTransportation=gameTransportation;  
            _gameFreeTime=gameFreeTime;
            _gameWorkAct=gameWorkAct;
    }   
    public Game()
    {
        
    }
}
}

