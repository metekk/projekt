using System.ComponentModel.DataAnnotations.Schema;
namespace DiplomskiClassLib;

public class Student {
    private int _studentID;
    private string _initials;
    private int _age;
    private int _teacherID;
    private int _difficultyType;
    private int _computerUse;
    private int _touchscreenUse;
    private int _degreeID;
    private int _motorSkills;
    private int _studentReading;
    private int _studentWriting;
    private int _studentMath;
    private int _studentTime;
    private int _studentMoney;
    private int _studentCommunication;
    private int _studentConversation;
    private int _studentLanguageUnderstanding;
    private int _studentLanguageProduction;
    private int _studentEmotions;
    private int _studentRules;
    private int _studentChangingActivities;
    private int _studentOtherPeople;
    private int _studentRiskSocSit;
    private int _studentManipulation;
    private int _studentFeeding;
    private int _studentDressing;
    private int _studentHygiene;
    private int _studentSchool;
    private int _studentTransportation;
    private int _studentWorkAct;
    private int _studentFreeTime;
    //private int _studentLearningOutcome;
    


    public int StudentID
    {
        get { return _studentID; }
        set { _studentID = value; }
    }
    public string Initials
    {
        get { return _initials; }
        set { _initials = value; }
    }

    public int Age
    {
        get { return _age; }
        set { _age = value; }
    }

    public int TeacherID { get; set; }

    public int DifficultyType
    {
        get { return _difficultyType; }
        set { _difficultyType = value; }
    }
    public int ComputerUse
    {
        get { return _computerUse; }
        set { _computerUse = value; }
    }
    public int TouchscreenUse
    {
        get { return _touchscreenUse; }
        set { _touchscreenUse = value; }
    }
    public int DegreeID
    {
        get { return _degreeID; }
        set { _degreeID = value; }
    }
    public int MotorSkills
    {
        get { return _motorSkills; }
        set { _motorSkills = value; }
    }
    public int StudentReading
    {
        get { return _studentReading; }
        set { _studentReading = value; }
    }
    public int StudentWriting
    {
        get { return _studentWriting; }
        set { _studentWriting = value; }
    }
    public int StudentMath
    {
        get { return _studentMath; }
        set { _studentMath = value; }
    }
    public int StudentTime
    {
        get { return _studentTime; }
        set { _studentTime = value; }
    }
    public int StudentMoney
    {
        get { return _studentMoney; }
        set { _studentMoney = value; }
    }
    public int StudentCommunication
    {
        get { return _studentCommunication; }
        set { _studentCommunication = value; }
    }
    public int StudentConversation
    {
        get { return _studentConversation; }
        set { _studentConversation = value; }
    }
    public int StudentLanguageUnderstanding
    {
        get { return _studentLanguageUnderstanding; }
        set { _studentLanguageUnderstanding = value; }
    }
    public int StudentLanguageProduction
    {
        get { return _studentLanguageProduction; }
        set { _studentLanguageProduction = value; }
    }
    public int StudentEmotions
    {
        get { return _studentEmotions; }
        set { _studentEmotions = value; }
    }

    public int StudentRules { get; set; }
    public int StudentChangingActivities { get; set; }
    public int StudentOtherPeople { get; set; }
    public int StudentRiskSocSit { get; set; }
    public int StudentManipulation { get; set; }
    public int StudentFeeding { get; set; }
    public int StudentDressing { get; set; }
    public int StudentHygiene { get; set; }
    public int StudentSchool { get; set; }
    public int StudentTransportation { get; set; }
    public int StudentWorkAct { get; set; }
    public int StudentFreeTime { get; set; }
    //public Teacher Teacher { get; set; }





    public Student(int studentID, string initials, int age, int teacherID, int difficultyType, int computerUse, int touchscreenUse, int degreeID,
        int motorSkills, int studentReading, int studentWriting, int studentMath, int studentTime, int studentMoney, int studentCommunication,
        int studentConversation, int studentLanguageUnderstanding, int studentLanguageProduction, int studentEmotions, int studentRules, 
        int studentChangingActivities, int studentOtherPeople, int studentRiskSocSit, int studentManipulation, int studentFeeding, int studentDressing,
        int studentHygiene, int studentSchool, int studentTransportation, int studentWorkAct, int studentFreeTime) {
        StudentID = studentID;
        Initials = initials;
        Age = age;
        TeacherID = teacherID;
        DifficultyType = difficultyType;
        ComputerUse = computerUse;
        TouchscreenUse = touchscreenUse;
        DegreeID = degreeID;
        MotorSkills = motorSkills;
        StudentReading = studentReading;
        StudentWriting = studentWriting;
        StudentMath = studentMath;
        StudentTime = studentTime;
        StudentMoney = studentMoney;
        StudentCommunication = studentCommunication;
        StudentConversation = studentConversation;
        StudentLanguageUnderstanding = studentLanguageUnderstanding;
        StudentLanguageProduction = studentLanguageProduction;
        StudentEmotions = studentEmotions;
        StudentRules = studentRules;
        StudentChangingActivities = studentChangingActivities;
        StudentOtherPeople = studentOtherPeople;
        StudentRiskSocSit = studentRiskSocSit;
        StudentManipulation = studentManipulation;
        StudentFeeding = studentFeeding;
        StudentDressing = studentDressing;
        StudentHygiene = studentHygiene;
        StudentSchool = studentSchool;
        StudentTransportation = studentTransportation;
        StudentWorkAct = studentWorkAct;
        StudentFreeTime = studentFreeTime;

        
    }

    public Student()
    {

    }
}