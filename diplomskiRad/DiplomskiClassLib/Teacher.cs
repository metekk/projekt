using System.Text.Json.Serialization;
namespace DiplomskiClassLib;

public class Teacher
{
    public int Id { get; set;}
    public string Name { get; set;}
    public string Email { get; set;}
    [JsonIgnore] public string Password { get; set;}
    public string School { get; set;}
    //public List<Student> Students {get;set;}

}