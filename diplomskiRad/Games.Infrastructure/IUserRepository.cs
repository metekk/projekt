using DiplomskiClassLib;
namespace Games.Infrastructure;

public interface IUserRepository{
    Teacher Create(Teacher teacher);   
    Teacher GetByEmail(string email);
    Teacher GetById(int id);
}