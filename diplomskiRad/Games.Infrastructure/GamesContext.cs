using Microsoft.EntityFrameworkCore;
using DiplomskiClassLib;
namespace Games.Infrastructure;
using Microsoft.AspNetCore.Http;
public class GamesContext : DbContext
{
    public GamesContext(DbContextOptions<GamesContext> options) : base(options)
    {

    }

    public DbSet<Game> Games { get; set; }
    public DbSet<Teacher> Teachers { get; set; }
    public DbSet<CommentGame> CommentGames { get; set; }
    public DbSet<Student> Students { get; set; }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Game>(entityTypeBuilder =>
        {
            entityTypeBuilder.HasKey(g => g.GameID);
            entityTypeBuilder.HasMany(g => g.Comments).WithOne().HasForeignKey(gc => gc.GameId);
        });

        modelBuilder.Entity<Teacher>(entityTypeBuilder =>
        {
            entityTypeBuilder.HasKey(t => t.Id);
            entityTypeBuilder.HasIndex(e => e.Email).IsUnique();
           // entityTypeBuilder.HasMany(g => g.Students).WithOne().HasForeignKey(gc => gc.StudentID);
        });

        modelBuilder.Entity<CommentGame>(entityTypeBuilder =>
        {
            entityTypeBuilder.HasKey(c => c.Id);
            entityTypeBuilder.Property(c => c.Id).ValueGeneratedOnAdd();
        });

         modelBuilder.Entity<Student>(entityTypeBuilder =>
        {
            entityTypeBuilder.HasKey(c => c.StudentID);
            entityTypeBuilder.Property(c => c.StudentID).ValueGeneratedOnAdd();
        });
    }

}
