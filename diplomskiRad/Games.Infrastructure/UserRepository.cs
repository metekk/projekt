using DiplomskiClassLib;
namespace Games.Infrastructure;

public class UserRepository : IUserRepository
{
    private readonly GamesContext _context;
    public UserRepository(GamesContext context)
    {
        _context = context;
    }

    public Teacher Create(Teacher teacher)
    {
        _context.Teachers.Add(teacher);
        teacher.Id = _context.SaveChanges();

        return teacher;
    }

    public Teacher GetByEmail(string email){
        return _context.Teachers.FirstOrDefault(u => u.Email == email);
    }
    public Teacher GetById(int id){
        return _context.Teachers.FirstOrDefault(u => u.Id == id);
    }
}