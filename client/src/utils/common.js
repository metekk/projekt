// return the user data from the session storage
export const getUser = () => {
    const userStr = sessionStorage.getItem('email');
    if (userStr) return JSON.parse(userStr);
    else return null;
  }
  
  // return the token from the session storage
  export const getToken = () => {
    return sessionStorage.getItem('password') || null;
  }
  
  // remove the token and user from the session storage
  export const removeUserSession = () => {
    sessionStorage.removeItem('password');
    sessionStorage.removeItem('email');
  }
  
  // set the token and user from the session storage
  export const setUserSession = (password, email, id) => {
    sessionStorage.setItem('password', password);
    sessionStorage.setItem('email', JSON.stringify(email));
    sessionStorage.setItem('id', id);
  }

  export const getId = () => {
    return sessionStorage.getItem('id') || null;
  }