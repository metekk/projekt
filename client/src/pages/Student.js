import React from "react";
import { Navbar } from "../components";
import { Footer } from "../containers";
import { Student }  from "../components";
import Addstudent from '../components/student/Addstudent';
import Studentlist from '../components/student/Studentlist';
import EditStudent from '../components/student/EditStudent';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import { Aboutus } from "../components";

const Stud = () => {
    return (
        <div className="App">
            <div className="gradient__bg">
                <Navbar></Navbar>
                <Student>
                    </Student>
            </div>
            <Footer></Footer>
        </div>
    )
}

export default Stud;

