import React from "react";
import { Navbar } from "../components";
import { Blog, Footer } from "../containers";
import {DataFetching} from '../components';
const Games=()=>{
    window.scrollTo(0,0);
    return(
        <div className="App">
            <div className="gradient__bg">
                <Navbar></Navbar>
                <DataFetching></DataFetching>
                <br></br><br></br>
            </div>
            <Footer></Footer>
        </div>
    )
}

export default Games;