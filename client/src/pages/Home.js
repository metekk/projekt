import React, {useEffect, useState} from "react";
import {Footer, Header, Games} from '../containers';
import {CTA, Navbar} from '../components';
import { AnimationOnScroll } from 'react-animation-on-scroll';

const Home=()=>{

    const [name, setName] = useState('');

    useEffect( () => {
        (
            async () => {
                const response = await fetch("http://localhost:5000/Auth/teacher", {
                    headers: {'Content-Type': 'application/json'},
                    credentials: 'include',
                });
                const content = await response.json();
                setName(content.name);
            }
        )();
    });

    return(
        <div className="App">
            <div className="gradient__bg">
                <Navbar></Navbar>
                <Header></Header>
            </div>
            <AnimationOnScroll animateIn="animate__bounceIn">
                <Games></Games>
            </AnimationOnScroll>
            <AnimationOnScroll animateIn="animate__bounceIn">
                <CTA></CTA>
            </AnimationOnScroll>
            <AnimationOnScroll animateIn="animate__bounceIn">
                <Footer></Footer>
            </AnimationOnScroll>
        </div>
    )
}

export default Home;