import React from "react";
import { Navbar } from "../components";
import { Footer } from "../containers";
import { Aboutus } from "../components";
const About=()=>{
    return(
        <div className="App">
            <div className="gradient__bg">
                <Navbar></Navbar>
                <Aboutus></Aboutus>
            </div>
            <Footer></Footer>
        </div>
    )
}

export default About;