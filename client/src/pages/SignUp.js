import React from "react";
import { LoginNavbar, SignUpForm } from "../components";

const SignUp=()=>{
    return(
        <div className="App">
            <LoginNavbar></LoginNavbar>
            <SignUpForm></SignUpForm>
      </div>
    )
}

export default SignUp;