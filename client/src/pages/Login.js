import React from "react";
import { LoginForm, LoginNavbar } from "../components";
import './login.css';

const Login=()=>{
    return(
        <div className="App">
            <LoginNavbar></LoginNavbar>
            <LoginForm></LoginForm>
      </div>
    )
}

export default Login;