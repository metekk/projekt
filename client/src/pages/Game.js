import React from "react";
import { Navbar } from "../components";
import { Blog, Footer } from "../containers";
import {Info} from "../components";
const Game=()=>{
    return(
        <div className="App">
            <div className="gradient__bg">
                <Navbar></Navbar>
                <Info></Info>
            </div>
            <Footer></Footer>
        </div>
    )
}

export default Game;