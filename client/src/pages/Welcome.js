import React from "react";
import { LoginNavbar } from "../components";
import {WelcomeHeader} from "../containers";

const Welcome=()=>{
    return(
        <div className="App">
            <LoginNavbar></LoginNavbar>
            <WelcomeHeader></WelcomeHeader>
        </div>
    );
};

export default Welcome;