import React from "react";
import { Navbar } from "../components";
import { Footer } from "../containers";
import {Contactus} from "../components";

const Contact=()=>{
    return(
        <div className="App">
            <div className="gradient__bg">
                <Navbar></Navbar>
                <Contactus></Contactus>
                <Footer></Footer>
            </div>
        </div>
    )
}

export default Contact;