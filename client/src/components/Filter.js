import React, {useEffect} from "react";
import './filter.css';

const Filter=({setActiveFilter, activeFilter, setFiltered, post})=>{

    useEffect(() =>{
        if(activeFilter === 0){
            setFiltered(post);
            return;
        }
        const filtered = post.filter((res) => res.gameField.includes(activeFilter));
        setFiltered(filtered);
    }, [activeFilter]);

    return(
        <div className="filter-container">
            <select>
                <option onClick={() => setActiveFilter(0)}>Sve</option>
                <option onClick={() => setActiveFilter("Crtanje")}>Crtanje</option>
                <option onClick={() => setActiveFilter("Boje")}>Boje</option>
                <option onClick={() => setActiveFilter("Brojevi")}>Brojevi</option>
                <option onClick={() => setActiveFilter("Čitanje")}>Čitanje</option>
                <option onClick={() => setActiveFilter("Pisanje")}>Pisanje</option>
            </select>
        </div>
    )
}

export default Filter;