﻿import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link, useParams, NavLink } from "react-router-dom";
import { Navbar } from "../../components";
import { Footer } from "../../containers";

const Algoritam = () => {

    const { id } = useParams();
    const [product, setProduct] = useState([]);
    const [loading, setLoading] = useState(false);
    const [games, setGames] = useState([]);


    useEffect(() => {
        const getProduct = async () => {
            setLoading(true);
            const response = await fetch(`http://localhost:5000/Student/Student-detail/${id}`);
            setProduct(await response.json());
            setLoading(false);
        }
        getProduct();
    }, []);

    const Loading = () => {
        return (
            <>
                Loading....
            </>
        )
    }

    useEffect(() => {
        const getGames = async () => {
            axios.get(`http://localhost:5000/GamesPage/games/${id}`)
                .then(res => {
                    console.log(res)
                    setGames(res.data)
                }).catch(err => {
                    console.log(err)
                });
        };

        getGames();
    }, []);


    const selectedGames = [];

    if (product.studentReading == 0) {
        for (let i = 0; i < games.gameID; i++) {
            if (games.field == 1 && games.gameReading == 1) {
                selectedGames[i] = games.gameID;
            }
        }


    }
    else {
       
}


    return (
        <div className="App">
            <div className="algoritmi">
                <Navbar></Navbar>
                <h3 className="PageHeading">Predložene igre za učenika</h3>

                <p>Identifikacijski broj učenika: {product.studentID}</p>
                <p>Inicijali učenika: {product.initials}</p>
                <p>Dob učenika: {product.age}</p>
                <p>Razina čitanja: {product.studentReading}</p>

                {games ? 
                        (games.length !== 0 ?
                        (
                            <div className='item-container'>
                                {games.map((post) =>(
                                    <div className="card" key={post.gameID}>
                                        <Link to={`/igrica/${post.gameID}`}><img src={post.filePath}></img></Link><br></br>
                                        <Link to={`/igrica/${post.gameID}`}><h3>{post.gameName}</h3><br></br> </Link>
                                        <p>{post.description}</p><br></br>
                                    <div className='link'>
                                        <Link to={`/igrica/${post.gameID}`}>Više</Link>
                                    </div>
                                    </div>
                                ))}
                            </div>)
                            : <h3>No data yet</h3>
                        )
                     : <h3>Predlažemo odabir igara iz područja svakodnevnog življenja</h3>}

                <p>Odabrane igre:</p>


            </div>
            <Footer></Footer>
        </div>

 );

};
export default Algoritam;