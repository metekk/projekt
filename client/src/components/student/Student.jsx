import React from "react";
import Addstudent from './Addstudent';
import Studentlist from './Studentlist';
import EditStudent from './EditStudent';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import './student.css';



const Student = () => (

        <div className="container">
            <nav className="navbar navbar-expand-lg navheader">
                <div className="collapse navbar-collapse" >
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link to={'/Addstudent'} className="nav-link">Dodaj ucenika</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/Studentlist'} className="nav-link">Popis mojih ucenika</Link>
                        </li>
                    </ul>
                </div>
            </nav> <br />
            <Routes>
                <Route exact path='/Addstudent' component={Addstudent} />
                <Route path='/edit/:id' component={EditStudent} />
                <Route path='/Studentlist' component={Studentlist} />
            </Routes>
        </div>
    
    
   
);

export default Student;

