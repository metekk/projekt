﻿import React, { useEffect, useState } from 'react';
import { Container, Col, Form, Row, FormGroup, Label, Input, Button } from 'reactstrap';
import axios from 'axios'
import './Addstudent.css';
import { Navbar } from "../../components";
import { Footer } from "../../containers";
import { useParams } from 'react-router-dom';



/*export function withRouter(Children) {
    return (props) => {

        const match = { params: useParams() };
        return <Children {...props} match={match} />
    }
}*/

const withRouter = WrappedComponent => props => {
    const params = useParams();
    // etc... other react-router-dom v6 hooks

    return (
        <WrappedComponent
            {...props}
            params={params}
        // etc...
        />
    );
};



class Edit extends React.Component {
    constructor(props) {
        super(props)


        this.onChangeInitials = this.onChangeInitials.bind(this);
        this.onChangeAge = this.onChangeAge.bind(this);
        this.onChangeDifficultyType = this.onChangeDifficultyType.bind(this);
        this.onChangeComputerUse = this.onChangeComputerUse.bind(this);
        this.onChangeTouchscreenUse = this.onChangeTouchscreenUse.bind(this);
        this.onChangeDegreeID = this.onChangeDegreeID.bind(this);
        this.onChangeMotorSkills = this.onChangeMotorSkills.bind(this);
        this.onChangeStudentReading = this.onChangeStudentReading.bind(this);
        this.onChangeStudentWriting = this.onChangeStudentWriting.bind(this);
        this.onChangeStudentMath = this.onChangeStudentMath.bind(this);
        this.onChangeStudentTime = this.onChangeStudentTime.bind(this);
        this.onChangeStudentMoney = this.onChangeStudentMoney.bind(this);
        this.onChangeStudentCommunication = this.onChangeStudentCommunication.bind(this);
        this.onChangeStudentConversation = this.onChangeStudentConversation.bind(this);
        this.onChangeStudentLanguageUnderstanding = this.onChangeStudentLanguageUnderstanding.bind(this);
        this.onChangeStudentLanguageProduction = this.onChangeStudentLanguageProduction.bind(this);
        this.onChangeStudentEmotions = this.onChangeStudentEmotions.bind(this);
        this.onChangeStudentRules = this.onChangeStudentRules.bind(this);
        this.onChangeStudentChangingActivities = this.onChangeStudentChangingActivities.bind(this);
        this.onChangeStudentOtherPeople = this.onChangeStudentOtherPeople.bind(this);
        this.onChangeStudentRiskSocSit = this.onChangeStudentRiskSocSit.bind(this);
        this.onChangeStudentManipulation = this.onChangeStudentManipulation.bind(this);
        this.onChangeStudentFeeding = this.onChangeStudentFeeding.bind(this);
        this.onChangeStudentDressing = this.onChangeStudentDressing.bind(this);
        this.onChangeStudentHygiene = this.onChangeStudentHygiene.bind(this);
        this.onChangeStudentSchool = this.onChangeStudentSchool.bind(this);
        this.onChangeStudentTransportation = this.onChangeStudentTransportation.bind(this);
        this.onChangeStudentWorkAct = this.onChangeStudentWorkAct.bind(this);
        this.onChangeStudentFreeTime = this.onChangeStudentFreeTime.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            initials: '',
            age: '',
            teacherID: '',
            difficultyType: '',
            computerUse: '',
            touchscreenUse: '',
            degreeID: '',
            motorSkills: '',
            studentReading: '',
            studentWriting: '',
            studentMath: '',
            studentTime: '',
            studentMoney: '',
            studentCommunication: '',
            studentConversation: '',
            studentLanguageUnderstanding: '',
            studentLanguageProduction: '',
            studentEmotions: '',
            studentRules: '',
            studentChangingActivities: '',
            studentOtherPeople: '',
            studentRiskSocSit: '',
            studentManipulation: '',
            studentFeeding: '',
            studentDressing: '',
            studentHygiene: '',
            studentSchool: '',
            studentTransportation: '',
            studentWorkAct: '',
            studentFreeTime: ''
        }
    }

    componentDidMount() {

        axios.get('http://localhost:5000/Student/StudentdetailById/' + this.props.params.id)
            .then(response => {
                this.setState({
                    initials: response.data.initials,
                    age: response.data.age,
                    difficultyType: response.data.difficultyType,
                    computerUse: response.data.computerUse,
                    touchscreenUse: response.data.touchscreenUse,
                    degreeID: response.data.degreeID,
                    motorSkills: response.data.motorSkills,
                    studentReading: response.data.studentReading,
                    studentWriting: response.data.studentWriting,
                    studentMath: response.data.studentMath,
                    studentTime: response.data.studentTime,
                    studentMoney: response.data.studentMoney,
                    studentCommunication: response.data.studentCommunication,
                    studentConversation: response.data.studentConversation,
                    studentLanguageUnderstanding: response.data.studentLanguageUnderstanding,
                    studentLanguageProduction: response.data.studentLanguageProduction,
                    studentEmotions: response.data.studentEmotions,
                    studentRules: response.data.studentRules,
                    studentChangingActivities: response.data.studentChangingActivities,
                    studentOtherPeople: response.data.studentOtherPeople,
                    studentRiskSocSit: response.data.studentRiskSocSit,
                    studentManipulation: response.data.studentManipulation,
                    studentFeeding: response.data.studentFeeding,
                    studentDressing: response.data.studentDressing,
                    studentHygiene: response.data.studentHygiene,
                    studentSchool: response.data.studentSchool,
                    studentTransportation: response.data.studentTransportation,
                    studentWorkAct: response.data.studentWorkAct,
                    studentFreeTime: response.data.studentFreeTime
                });
                document.getElementsByName("initials")[0].value = response.data.initials
                document.getElementsByName("age")[0].value = response.data.age
                document.getElementsByName("difficultyType")[0].value = response.data.difficultyType
                document.getElementsByName("computerUse")[0].value = response.data.computerUse
                document.getElementsByName("touchscreenUse")[0].value = response.data.touchscreenUse
                document.getElementsByName("degreeID")[0].value = response.data.degreeID
                document.getElementsByName("motorSkills")[0].value = response.data.motorSkills
                document.getElementsByName("studentReading")[0].value = response.data.studentReading
                document.getElementsByName("studentWriting")[0].value = response.data.studentWriting
                document.getElementsByName("studentMath")[0].value = response.data.studentMath
                document.getElementsByName("studentTime")[0].value = response.data.studentTime
                document.getElementsByName("studentMoney")[0].value = response.data.studentMoney
                document.getElementsByName("studentCommunication")[0].value = response.data.studentCommunication
                document.getElementsByName("studentConversation")[0].value = response.data.studentConversation
                document.getElementsByName("studentLanguageUnderstanding")[0].value = response.data.studentLanguageUnderstanding
                document.getElementsByName("studentLanguageProduction")[0].value = response.data.studentLanguageProduction
                document.getElementsByName("studentEmotions")[0].value = response.data.studentEmotions
                document.getElementsByName("studentRules")[0].value = response.data.studentRules
                document.getElementsByName("studentChangingActivities")[0].value = response.data.studentChangingActivities
                document.getElementsByName("studentOtherPeople")[0].value = response.data.studentOtherPeople
                document.getElementsByName("studentRiskSocSit")[0].value = response.data.studentRiskSocSit
                document.getElementsByName("studentManipulation")[0].value = response.data.studentManipulation
                document.getElementsByName("studentFeeding")[0].value = response.data.studentFeeding
                document.getElementsByName("studentDressing")[0].value = response.data.studentDressing
                document.getElementsByName("studentHygiene")[0].value = response.data.studentHygiene
                document.getElementsByName("studentSchool")[0].value = response.data.studentSchool
                document.getElementsByName("studentTransportation")[0].value = response.data.studentTransportation
                document.getElementsByName("studentWorkAct")[0].value = response.data.studentWorkAct
                document.getElementsByName("studentFreeTime")[0].value = response.data.studentFreeTime
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onChangeInitials(e) {
        this.setState({
            initials: e.target.value
        });
    }
    onChangeAge(e) {
        this.setState({
            age: e.target.value
        });
    }
    onChangeDifficultyType(e) {
        this.setState({
            difficultyType: e.target.value
        });
    }
    onChangeComputerUse(e) {
        this.setState({
            computerUse: e.target.value
        });
    }
    onChangeTouchscreenUse(e) {
        this.setState({
            touchscreenUse: e.target.value
        });
    }
    onChangeDegreeID(e) {
        this.setState({
            degreeID: e.target.value
        });
    }
    onChangeMotorSkills(e) {
        this.setState({
            motorSkills: e.target.value
        });
    }
    onChangeStudentReading(e) {
        this.setState({
            studentReading: e.target.value
        });
    }
    onChangeStudentWriting(e) {
        this.setState({
            studentWriting: e.target.value
        });
    }
    onChangeStudentMath(e) {
        this.setState({
            studentMath: e.target.value
        });
    }
    onChangeStudentTime(e) {
        this.setState({
            studentTime: e.target.value
        });
    }
    onChangeStudentMoney(e) {
        this.setState({
            studentMoney: e.target.value
        });
    }
    onChangeStudentCommunication(e) {
        this.setState({
            studentCommunication: e.target.value
        });
    }
    onChangeStudentConversation(e) {
        this.setState({
            studentConversation: e.target.value
        });
    }
    onChangeStudentLanguageUnderstanding(e) {
        this.setState({
            studentLanguageUnderstanding: e.target.value
        });
    }
    onChangeStudentLanguageProduction(e) {
        this.setState({
            studentLanguageProduction: e.target.value
        });
    }
    onChangeStudentEmotions(e) {
        this.setState({
            studentEmotions: e.target.value
        });
    }
    onChangeStudentRules(e) {
        this.setState({
            studentRules: e.target.value
        });
    }
    onChangeStudentChangingActivities(e) {
        this.setState({
            studentChangingActivities: e.target.value
        });
    }
    onChangeStudentOtherPeople(e) {
        this.setState({
            studentOtherPeople: e.target.value
        });
    }
    onChangeStudentRiskSocSit(e) {
        this.setState({
            studentRiskSocSit: e.target.value
        });
    }
    onChangeStudentManipulation(e) {
        this.setState({
            studentManipulation: e.target.value
        });
    }
    onChangeStudentFeeding(e) {
        this.setState({
            studentFeeding: e.target.value
        });
    }
    onChangeStudentDressing(e) {
        this.setState({
            studentDressing: e.target.value
        });
    }
    onChangeStudentHygiene(e) {
        this.setState({
            studentHygiene: e.target.value
        });
    }
    onChangeStudentSchool(e) {
        this.setState({
            studentSchool: e.target.value
        });
    }
    onChangeStudentTransportation(e) {
        this.setState({
            studentTransportation: e.target.value
        });
    }
    onChangeStudentWorkAct(e) {
        this.setState({
            studentWorkAct: e.target.value
        });
    }
    onChangeStudentFreeTime(e) {
        this.setState({
            studentFreeTime: e.target.value
        });
    }
    




    onSubmit(e) {
        // console.log(this.props.params.id)    
        // debugger;
        e.preventDefault();
        const obj = {
            studentID: this.props.params.id,
            initials: this.state.initials,
            age: this.state.age,
            difficultyType: this.state.difficultyType,
            computerUse: this.state.computerUse,
            touchscreenUse: this.state.touchscreenUse,
            degreeID: this.state.degreeID,
            motorSkills: this.state.motorSkills,
            studentReading: this.state.studentReading,
            studentWriting: this.state.studentWriting,
            studentMath: this.state.studentMath,
            studentTime: this.state.studentTime,
            studentMoney: this.state.studentMoney,
            studentCommunication: this.state.studentCommunication,
            studentConversation: this.state.studentConversation,
            studentLanguageUnderstanding: this.state.studentLanguageUnderstanding,
            studentLanguageProduction: this.state.studentLanguageProduction,
            studentEmotions: this.state.studentEmotions,
            studentRules: this.state.studentRules,
            studentChangingActivities: this.state.studentChangingActivities,
            studentOtherPeople: this.state.studentOtherPeople,
            studentRiskSocSit: this.state.studentRiskSocSit,
            studentManipulation: this.state.studentManipulation,
            studentFeeding: this.state.studentFeeding,
            studentDressing: this.state.studentDressing,
            studentHygiene: this.state.studentHygiene,
            studentSchool: this.state.studentSchool,
            studentTransportation: this.state.studentTransportation,
            studentWorkAct: this.state.studentWorkAct,
            studentFreeTime: this.state.studentFreeTime
        };
        // console.log(obj)
        axios.put('http://localhost:5000/Student/update-student/' + this.props.params.id, obj)
            .then(res => window.location.replace("/Studentlist"));
        // debugger;
        // this.props.history.push('/Studentlist')
    }
    render() {
        return (
            <Container className="App">
                <div className="gradient__bg">
                    <Navbar></Navbar>
                <h4 className="PageHeading">Update Student Informations</h4>
                <Form className="form" onSubmit={this.onSubmit}>
                    <Col>
                            <FormGroup row>
                                <Label for="initials" sm={2}>Inicijali učenika</Label>
                                <Col sm={3}>
                                    <Input type="text" name="initials" value={this.state.Initials} onChange={this.onChangeInitials} placeholder="Unestide inicijale učenika"/>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="age" sm={2}>Dob</Label>
                                <Col sm={2}>
                                    <Input type="number" name="age" onChange={this.onChangeAge} value={this.state.Age} placeholder="Unestide dob učenika" />
                                </Col>
                            </FormGroup>

                          

                            <FormGroup row>
                                <Label for="difficultyType" sm={2}>Vrsta teškoće</Label>
                                <Col sm={3}>
                                    <select name="difficultyType" value={this.state.value} onChange={this.onChangeDifficultyType}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="1">oštećenje vida</option>
                                        <option value="2">oštećenje sluha</option>
                                        <option value="3">oštećenje jezično-govorne-glasovne komunikacije i specifične teškoće u učenju</option>
                                        <option value="4">oštećenje organa</option>
                                        <option value="5">intelektualne teškoće</option>
                                        <option value="6">poremećaji u ponašanju</option>
                                        <option value="7">udružene teškoće</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="computerUse" sm={2}>Razina korištenja računala</Label>
                                <Col sm={3}>
                                    <select name="computerUse" value={this.state.value} onChange={this.onChangeComputerUse}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">ne zna koristiti računalo</option>
                                        <option value="1">koristi bazične funkcionalnosti (korištenje programa uz pomoć druge osobe)</option>
                                        <option value="2">koristi napredne funkcionalnosti (samostalno pretraživanje weba)</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="touchscreenUse" sm={2}>Razina korištenja dodirnika</Label>
                                <Col sm={3}>
                                    <select name="touchscreenUse" value={this.state.value} onChange={this.onChangeTouchscreenUse}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">ne zna koristiti uređaje</option>
                                        <option value="1">koristi bazične funkcionalnosti (korištenje aplikacija uz pomoć druge osobe)</option>
                                        <option value="2">koristi napredne funkcionalnosti (samostalno korištenje aplikacija)</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="degreeID" sm={2}>Stupanj intelektualnih teškoća prema razini podrške</Label>
                                <Col sm={3}>
                                    <select name="degreeID" value={this.state.value} onChange={this.onChangeDegreeID}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="1">duboke intelektualne teškoće</option>
                                        <option value="2">teške intelektualne teškoće</option>
                                        <option value="3">umjerene intelektualne teškoće</option>
                                        <option value="4">blage intelektualne teškoće</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="motorSkills" sm={2}>Fina motorika</Label>
                                <Col sm={3}>
                                    <select name="motorSkills" value={this.state.value} onChange={this.onChangeMotorSkills}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">motorički tremor ruku</option>
                                        <option value="1">hvata cijelom rukom ili ima nezreli pincet hvat</option>
                                        <option value="2">pokazuje na točke, zrna</option>
                                        <option value="3">očuvana fina motorika (zreli pincet hvat)</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <h4 className="CategoryHeading">Konceptualna domena</h4>
                            <FormGroup row>
                                <Label for="studentReading" sm={2}>Stupanj vještine čitanja</Label>
                                <Col sm={5}>
                                    <select name="studentReading" value={this.state.value} onChange={this.onChangeStudentReading}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik nema razvijene predčitalačke vještine</option>
                                        <option value="1">učenik ima razvijene predčitalačke vještine (razlikuje tekst od slike,  prati tekst s lijeva na desno, globalno čita, uočava prvi i zadnji glas u riječi, rastavlja na slogove, svjestan je rime...)</option>
                                        <option value="21">učenik usvaja tehniku čitanja slovkajući i/ili čita slogove</option>
                                        <option value="22">učenik usvaja tehniku čitanja  kraćih i dužih riječi</option>
                                        <option value="23">učenik usvaja tehniku čitanja rečenica</option>
                                        <option value="3">učenik ima razvijenu tehniku čitanja bez razumijevanja pročitanog</option>
                                        <option value="4">učenik čita s razumijevanjem</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentWriting" sm={2}>Stupanj vještine pisanja</Label>
                                <Col sm={5}>
                                    <select name="studentWriting" value={this.state.value} onChange={this.onChangeStudentWriting}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik nema razvijenu vještinu pisanja</option>
                                        <option value="1">učenik prepisuje slova prema predlošku</option>
                                        <option value="21">učenik je usvojio ili usvaja tehniku pisanja po diktatu: piše prva slova (samoglasnike, učestala slova)</option>
                                        <option value="22">učenik je usvojio ili usvaja tehniku pisanja po diktatu: piše riječi</option>
                                        <option value="23">učenik je usvojio ili usvaja tehniku pisanja po diktatu: piše rečenice</option>
                                        <option value="3">učenik piše samostalno formalnim pismom (tiskana slova)</option>
                                        <option value="4">učenik piše samostalno rukopisom (pisana slova)</option>
                                        <option value="9">učenik piše na uređaju (tablet, prijenosno računalo)</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentMath" sm={2}>Stupanj matematičkih vještina</Label>
                                <Col sm={5}>
                                    <select name="studentMath" value={this.state.value} onChange={this.onChangeStudentMath}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik nema razvijene predmatematičke vještine (svojstva predmeta, razvrstavanje, odnosi u prostoru, veze između predmeta, kvalitativni i kvantitativni odnosi, geometrijski oblici, količina, slijed)</option>
                                        <option value="1">učenik ima razvijene predmatematičke vještine (svojstva predmeta, razvrstavanje, odnosi u prostoru, veze između predmeta, kvalitativni i kvantitativni odnosi, geometrijski oblici, količina, slijed)</option>
                                        <option value="21">učenik je usvojio ili usvaja osnovne matematičke kompetencije: pojam brojeva do 5 (simbol, količina, imenovanje, slijed)</option>
                                        <option value="22">učenik je usvojio ili usvaja osnovne matematičke kompetencije: pojam brojeva do 10 (simbol, količina, imenovanje, slijed)</option>
                                        <option value="23">učenik je usvojio ili usvaja osnovne matematičke kompetencije: pojam brojeva do 20 (simbol, količina, imenovanje, slijed)</option>
                                        <option value="24">učenik je usvojio ili usvaja osnovne matematičke kompetencije: pojam brojeva do 100 (simbol, količina, imenovanje, slijed)</option>
                                        <option value="25">učenik je usvojio ili usvaja osnovne matematičke kompetencije: pojam brojeva više od 100 (simbol, količina, imenovanje, slijed)</option>
                                        <option value="31">učenik razumije računske radnje zbrajanja i oduzimanja: računa uz konkrete</option>
                                        <option value="32">učenik razumije računske radnje zbrajanja i oduzimanja: računa uz pomoć kalkulatora</option>
                                        <option value="4">učenik računa apstraktno (rješava jednostavne matematičke priče)</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentTime" sm={2}>Stupanj vještina snalaženja u vremenu (priroda i društvo)</Label>
                                <Col sm={5}>
                                    <select name="studentTime" value={this.state.value} onChange={this.onChangeStudentTime}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik nema razvijene vještine snalaženja u vremenu</option>
                                        <option value="1">učenik samostalno ili uz pomoć određuje i razumije doba dana (jutro, prijepodne, podne, poslijepodne, večer, noć)</option>
                                        <option value="2">učenik samostalno ili uz pomoć imenuje i razumije godišnja doba</option>
                                        <option value="3">učenik samostalno ili uz pomoć imenuje i razumije dane u tjednu</option>
                                        <option value="4">učenik samostalno ili uz pomoć određuje i razumije vremenske pojmove danas, jučer, sutra (prije/poslije)</option>
                                        <option value="5">učenik samostalno ili uz pomoć imenuje i razumije mjesece u godini</option>
                                        <option value="6">učenik samostalno ili uz pomoć očitava vrijeme na analognom satu (satu s kazaljkama)</option>
                                        <option value="7">učenik samostalno ili uz pomoć određuje i poznaje datum na kalendaru</option>
                                        <option value="8">učenik u potpunosti ima razvijene sve navedene vještine snalaženja u vremenu</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentMoney" sm={2}>Stupanj vještine upravljanja novcem</Label>
                                <Col sm={5}>
                                    <select name="studentMoney" value={this.state.value} onChange={this.onChangeStudentMoney}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik nema razvijene vještine upravljanja novcem</option>
                                        <option value="1">učenik prepoznaje novac kao predmet</option>
                                        <option value="2">učenik razlikuje kovanice od novčanica</option>
                                        <option value="3">učenik određuje vrijednost novca pojedinačno</option>
                                        <option value="4">učenik razumije uporabnu vrijednost novca</option>
                                        <option value="5">učenik samostalno ili uz minimalnu pomoć obavlja kupovinu</option>
                                    </select>
                                </Col>
                            </FormGroup>
                            <h4 className="CategoryHeading">Socijalna domena</h4>
                            <FormGroup row>
                                <Label for="studentCommunication" sm={2}>Stupanj ekspresivne komunikacije</Label>
                                <Col sm={4}>
                                    <select name="studentCommunication" value={this.state.value} onChange={this.onChangeStudentCommunication}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik ne komunicira smisleno (ni neverbalno ni verbalno)</option>
                                        <option value="1">učenik komunicira neverbalno putem mimike i geste ili putem potpomognutih oblika komunikacije (PECS, komunikatori…)</option>
                                        <option value="2">učenik komunicira verbalno oskudno s ili bez potpomognutih oblika komunikacije (PECS, komunikatori…)</option>
                                        <option value="3">učenik ima razvijenu optimalnu verbalnu komunikaciju</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentConversation" sm={2}>Stupanj konverzacije (sporazumijevanja)</Label>
                                <Col sm={4}>
                                    <select name="studentConversation" value={this.state.value} onChange={this.onChangeStudentConversation}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">konverzacija s učenikom značajno otežana</option>
                                        <option value="1">konverzacija s učenikom moguća je uglavnom o temama iz njegove uobičajene socijalne situacije</option>
                                        <option value="2">konverzacija s učenikom je složenija i uključuje teme izvan njegove uobičajene socijalne situacije</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentLanguageUnderstanding" sm={2}>Stupanj jezičkog razumijevanja</Label>
                                <Col sm={3}>
                                    <select name="studentLanguageUnderstanding" value={this.state.value} onChange={this.onChangeStudentLanguageUnderstanding}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">izostanak ili vrlo ograničeno jezično razumijevanje</option>
                                        <option value="1">jezično razumijevanje gramatički i sadržajno jednostavnih rečenica/uputa</option>
                                        <option value="2">jezično razumijevanje gramatički i sadržajno složenijih rečenica/uputa</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentLanguageProduction" sm={2}>Stupanj jezične proizvodnje</Label>
                                <Col sm={3}>
                                    <select name="studentLanguageProduction" value={this.state.value} onChange={this.onChangeStudentLanguageProduction}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">izostanak ili vrlo ograničena jezična proizvodnja</option>
                                        <option value="1">proizvodnja jednočlanih ili dvočlanih iskaza</option>
                                        <option value="2">proizvodnja jednostavnih iskaza (jednostavnih rečenica)</option>
                                        <option value="3">proizvodnja složenih rečeničnih iskaza</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentEmotions" sm={2}>Stupanj reguliranja emocija</Label>
                                <Col sm={3}>
                                    <select name="studentEmotions" value={this.state.value} onChange={this.onChangeStudentEmotions}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">često prisutne teškoće reguliranja emocija</option>
                                        <option value="1">osrednje prisutne teškoće u regulaciji emocija</option>
                                        <option value="2">rijetko prisutne teškoće u regulaciji emocija</option>
                                        <option value="3">nisu prisutne teškoće u regulaciji emocija</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentRules" sm={2}>Mogućnost slijeđenja pravila</Label>
                                <Col sm={3}>
                                    <select name="studentRules" value={this.state.value} onChange={this.onChangeStudentRules}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">često prisutne teškoće u slijeđenju dogovorenih pravila</option>
                                        <option value="1">osrednje prisutne teškoće u slijeđenju dogovorenih pravila</option>
                                        <option value="2">rijetko prisutne teškoće u slijeđenju dogovorenih pravila</option>
                                        <option value="3">nisu prisutne teškoće u slijeđenju dogovorenih pravila</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentChangingActivities" sm={2}>Mogućnost prelaska s aktivnosti na aktivnost</Label>
                                <Col sm={3}>
                                    <select name="studentChangingActivities" value={this.state.value} onChange={this.onChangeStudentChangingActivities}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">često prisutne teškoće u prelasku s aktivnosti na aktivnost</option>
                                        <option value="1">osrednje prisutne teškoće u prelasku s aktivnosti na aktivnost</option>
                                        <option value="2">rijetko prisutne teškoće u prelasku s aktivnosti na aktivnost</option>
                                        <option value="3">nisu prisutne teškoće u prelasku s aktivnosti na aktivnost</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentOtherPeople" sm={2}>Ponašanje u ovisnosti o drugim osobama u okruženju</Label>
                                <Col sm={3}>
                                    <select name="studentOtherPeople" value={this.state.value} onChange={this.onChangeStudentOtherPeople}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">veće promjene u ponašanju u ovisnosti o drugim osobama u okruženju</option>
                                        <option value="1">osrednje promjene u ponašanju u ovisnosti o drugim osobama u okruženju</option>
                                        <option value="2">manje promjene u ponašanju u ovisnosti o drugim osobama u okruženju</option>
                                        <option value="3">ne postoji promjena u ponašanju u ovisnosti o drugim osobama u okruženju</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentRiskSocSit" sm={2}>Razumijevanje rizičnih socijalnih situacija</Label>
                                <Col sm={3}>
                                    <select name="studentRiskSocSit" value={this.state.value} onChange={this.onChangeStudentRiskSocSit}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">često prisutne teškoće u razumijevanju rizičnih socijalnih situacija</option>
                                        <option value="1">osrednje prisutne teškoće u razumijevanju rizičnih socijalnih situacija</option>
                                        <option value="2">rijetko prisutne teškoće u razumijevanju rizičnih socijalnih situacija</option>
                                        <option value="3">nisu prisutne teškoće u razumijevanju rizičnih socijalnih situacija</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentManipulation" sm={2}>Rizik za manipulaciju od drugih</Label>
                                <Col sm={3}>
                                    <select name="studentManipulation" value={this.state.value} onChange={this.onChangeStudentManipulation}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">često je sklon/a slijediti sugerirano ponašanje vršnjaka ne vodeći računa o svojoj dobrobiti</option>
                                        <option value="1">osrednje je sklon/a slijediti sugerirano ponašanje vršnjaka ne vodeći računa o svojoj dobrobiti</option>
                                        <option value="2">rijetko je sklon/a slijediti sugerirano ponašanje vršnjaka ne vodeći računa o svojoj dobrobiti</option>
                                        <option value="3">uopće nije sklon slijediti sugerirano ponašanje vršnjaka ne vodeći računa o svojoj dobrobiti</option>
                                    </select>
                                </Col>
                            </FormGroup>
                            <h4 className="CategoryHeading">Praktična domena - aktivnosti svakodnevnog života</h4>
                            <FormGroup row>
                                <Label for="studentFeeding" sm={2}>Podrška u hranjenju</Label>
                                <Col sm={3}>
                                    <select name="studentFeeding" value={this.state.value} onChange={this.onChangeStudentFeeding}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentDressing" sm={2}>Podrška u oblačenju</Label>
                                <Col sm={3}>
                                    <select name="studentDressing" value={this.state.value} onChange={this.onChangeStudentDressing}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentHygiene" sm={2}>Podrška u obavljanju osobne higijene</Label>
                                <Col sm={3}>
                                    <select name="studentHygiene" value={this.state.value} onChange={this.onChangeStudentHygiene}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentSchool" sm={2}>Podrška pri odlasku u školu</Label>
                                <Col sm={3}>
                                    <select name="studentSchool" value={this.state.value} onChange={this.onChangeStudentSchool}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentTransportation" sm={2}>Podrška prilikom korištenja sredstva javnog prijevoza</Label>
                                <Col sm={3}>
                                    <select name="studentTransportation" value={this.state.value} onChange={this.onChangeStudentTransportation}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentWorkAct" sm={2}>Podrška pri obavljanju svakodnevnih radnih aktivnosti kao npr. čišćenje, priprema jela, postavljanje stola...</Label>
                                <Col sm={3}>
                                    <select name="studentWorkAct" value={this.state.value} onChange={this.onChangeStudentWorkAct}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentFreeTime" sm={2}>Podrška pri provođenju slobodnog vremena</Label>
                                <Col sm={3}>
                                    <select name="studentFreeTime" value={this.state.value} onChange={this.onChangeStudentFreeTime}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0"> izrazite teškoće u osmišljavanju aktivnosti slobodnog vremena</option>
                                        <option value="1">često potrebna podrška u osmišljavanju aktivnosti slobodnog vremena</option>
                                        <option value="2">osrednje potrebna podrška u osmišljavanju aktivnosti slobodnog vremena</option>
                                        <option value="3">rijetko potrebna ili nepotrebna podrška u osmišljavanju aktivnosti slobodnog vremena</option>
                                    </select>
                                </Col>
                            </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup row>
                            <Col sm={5}>
                            </Col>
                            <Col sm={1}>
                                <Button type="submit" color="success">Submit</Button>{' '}
                            </Col>
                            <Col sm={1}>
                                <Button color="danger">Cancel</Button>{' '}
                            </Col>
                            <Col sm={5}>
                            </Col>
                        </FormGroup>
                    </Col>
                    </Form>
                </div>
                <Footer></Footer>
            </Container>
        );
    }

}

//export default Edit;
export default withRouter(Edit);