﻿import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Navbar } from "../../components";
import { Footer } from "../../containers";
import './Addstudent.css';
import { Container, Col, Form, Row, FormGroup, Label, Input, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { getId } from '../../utils/common.js';



class Addstudent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            initials: '',
            age: '',
            teacherID: '',
            difficultyType: '',
            computerUse: '',
            touchscreenUse: '',
            degreeID: '',
            motorSkills: '',
            studentReading: '',
            studentWriting: '',
            studentMath: '',
            studentTime: '',
            studentMoney: '',
            studentCommunication: '',
            studentConversation: '',
            studentLanguageUnderstanding: '',
            studentLanguageProduction: '',
            studentEmotions: '',
            studentRules: '',
            studentChangingActivities: '',
            studentOtherPeople: '',
            studentRiskSocSit: '',
            studentManipulation: '',
            studentFeeding: '',
            studentDressing: '',
            studentHygiene: '',
            studentSchool: '',
            studentTransportation: '',
            studentWorkAct: '',
            studentFreeTime: ''
        }

    }
    
    Addstudent = () => {
        axios.post('http://localhost:5000/Student/new-student/', {
            initials: this.state.initials,
            age: this.state.age,
            teacherID: getId(),
            difficultyType: this.state.difficultyType,
            computerUse: this.state.computerUse,
            touchscreenUse: this.state.touchscreenUse,
            degreeID: this.state.degreeID,
            motorSkills: this.state.motorSkills,
            studentReading: this.state.studentReading,
            studentWriting: this.state.studentWriting,
            studentMath: this.state.studentMath,
            studentTime: this.state.studentTime,
            studentMoney: this.state.studentMoney,
            studentCommunication: this.state.studentCommunication,
            studentConversation: this.state.studentConversation,
            studentLanguageUnderstanding: this.state.studentLanguageUnderstanding,
            studentLanguageProduction: this.state.studentLanguageProduction,
            studentEmotions: this.state.studentEmotions,
            studentRules: this.state.studentRules,
            studentChangingActivities: this.state.studentChangingActivities,
            studentOtherPeople: this.state.studentOtherPeople,
            studentRiskSocSit: this.state.studentRiskSocSit,
            studentManipulation: this.state.studentManipulation,
            studentFeeding: this.state.studentFeeding,
            studentDressing: this.state.studentDressing,
            studentHygiene: this.state.studentHygiene,
            studentSchool: this.state.studentSchool,
            studentTransportation: this.state.studentTransportation,
            studentWorkAct: this.state.studentWorkAct,
            studentFreeTime: this.state.studentFreeTime
        }).then(res => {
            if (res.status=="200") {
                console.log(res);
                console.log(res.data);
                alert('Učenik unesen.');
                window.location.replace("/Studentlist");
            }
            else {
                console.log(res.data.Status);
                alert('Dogodila se greška, molim pokušajte ponovno');
                debugger;
            }
        })
    }

    handleChange = (e) => {
        this.setState(
            { [e.target.name]: e.target.value });
    }



    render() {
        return (

            <div className="App">
                <div className="gradient__bg">
                <Navbar></Navbar>
                <h3 className="PageHeading">Unos podataka o učeniku</h3>
                    <Form className="form">
                        <h4 className="CategoryHeading">Opće informacije</h4>
                    <Col>
                        <FormGroup row>
                            <Label for="initials" sm={2}>Inicijali učenika</Label>
                            <Col sm={3}>
                                <Input type="text" name="initials" onChange={this.handleChange} value={this.state.initials} placeholder="Unesite inicijale učenika" />
                            </Col>
                         </FormGroup>

                        <FormGroup row>
                            <Label for="age" sm={2}>Dob</Label>
                            <Col sm={2}>
                                <Input type="number" name="age" onChange={this.handleChange} value={this.state.age} placeholder="Unestide dob učenika" />
                            </Col>
                        </FormGroup>

                        {/* <FormGroup row>
                            <Label for="teacherID" sm={2}>Šifra nastavnika</Label>
                            <Col sm={2}>
                                <Input type="text" name="teacherID" onChange={this.handleChange} value={this.state.teacherID} placeholder="Unesite Vašu šifru" />
                            </Col>
                        </FormGroup> */}

                            <FormGroup row>
                                <Label for="difficultyType" sm={2}>Vrsta teškoće</Label>
                                <Col sm={3}>
                                    <select name="difficultyType" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="1">oštećenje vida</option>
                                        <option value="2">oštećenje sluha</option>
                                        <option value="3">oštećenje jezično-govorne-glasovne komunikacije i specifične teškoće u učenju</option>
                                        <option value="4">oštećenje organa</option>
                                        <option value="5">intelektualne teškoće</option>
                                        <option value="6">poremećaji u ponašanju</option>
                                        <option value="7">udružene teškoće</option>
                                         </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="computerUse" sm={2}>Razina korištenja računala</Label>
                                <Col sm={3}>
                                    <select name="computerUse" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">ne zna koristiti računalo</option>
                                        <option value="1">koristi bazične funkcionalnosti (korištenje programa uz pomoć druge osobe)</option>
                                        <option value="2">koristi napredne funkcionalnosti (samostalno pretraživanje weba)</option> 
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="touchscreenUse" sm={2}>Razina korištenja dodirnika</Label>
                                <Col sm={3}>
                                    <select name="touchscreenUse" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">ne zna koristiti uređaje</option>
                                        <option value="1">koristi bazične funkcionalnosti (korištenje aplikacija uz pomoć druge osobe)</option>
                                        <option value="2">koristi napredne funkcionalnosti (samostalno korištenje aplikacija)</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="degreeID" sm={2}>Stupanj intelektualnih teškoća prema razini podrške</Label>
                                <Col sm={3}>
                                    <select name="degreeID" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="1">duboke intelektualne teškoće</option>
                                        <option value="2">teške intelektualne teškoće</option>
                                        <option value="3">umjerene intelektualne teškoće</option>
                                        <option value="4">blage intelektualne teškoće</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="motorSkills" sm={2}>Fina motorika</Label>
                                <Col sm={3}>
                                    <select name="motorSkills" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">motorički tremor ruku</option>
                                        <option value="1">hvata cijelom rukom ili ima nezreli pincet hvat</option>
                                        <option value="2">pokazuje na točke, zrna</option>
                                        <option value="3">očuvana fina motorika (zreli pincet hvat)</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <h4 className="CategoryHeading">Konceptualna domena</h4>
                            <FormGroup row>
                                <Label for="studentReading" sm={2}>Stupanj vještine čitanja</Label>
                                <Col sm={5}>
                                    <select name="studentReading" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik nema razvijene predčitalačke vještine</option>
                                        <option value="1">učenik ima razvijene predčitalačke vještine (razlikuje tekst od slike,  prati tekst s lijeva na desno, globalno čita, uočava prvi i zadnji glas u riječi, rastavlja na slogove, svjestan je rime...)</option>
                                        <option value="21">učenik usvaja tehniku čitanja slovkajući i/ili čita slogove</option>
                                        <option value="22">učenik usvaja tehniku čitanja  kraćih i dužih riječi</option>
                                        <option value="23">učenik usvaja tehniku čitanja rečenica</option>
                                        <option value="3">učenik ima razvijenu tehniku čitanja bez razumijevanja pročitanog</option>
                                        <option value="4">učenik čita s razumijevanjem</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentWriting" sm={2}>Stupanj vještine pisanja</Label>
                                <Col sm={5}>
                                    <select name="studentWriting" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik nema razvijenu vještinu pisanja</option>
                                        <option value="1">učenik prepisuje slova prema predlošku</option>
                                        <option value="21">učenik je usvojio ili usvaja tehniku pisanja po diktatu: piše prva slova (samoglasnike, učestala slova)</option>
                                        <option value="22">učenik je usvojio ili usvaja tehniku pisanja po diktatu: piše riječi</option>
                                        <option value="23">učenik je usvojio ili usvaja tehniku pisanja po diktatu: piše rečenice</option>
                                        <option value="3">učenik piše samostalno formalnim pismom (tiskana slova)</option>
                                        <option value="4">učenik piše samostalno rukopisom (pisana slova)</option>
                                        <option value="9">učenik piše na uređaju (tablet, prijenosno računalo)</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentMath" sm={2}>Stupanj matematičkih vještina</Label>
                                <Col sm={5}>
                                    <select name="studentMath" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik nema razvijene predmatematičke vještine (svojstva predmeta, razvrstavanje, odnosi u prostoru, veze između predmeta, kvalitativni i kvantitativni odnosi, geometrijski oblici, količina, slijed)</option>
                                        <option value="1">učenik ima razvijene predmatematičke vještine (svojstva predmeta, razvrstavanje, odnosi u prostoru, veze između predmeta, kvalitativni i kvantitativni odnosi, geometrijski oblici, količina, slijed)</option>
                                        <option value="21">učenik je usvojio ili usvaja osnovne matematičke kompetencije: pojam brojeva do 5 (simbol, količina, imenovanje, slijed)</option>
                                        <option value="22">učenik je usvojio ili usvaja osnovne matematičke kompetencije: pojam brojeva do 10 (simbol, količina, imenovanje, slijed)</option>
                                        <option value="23">učenik je usvojio ili usvaja osnovne matematičke kompetencije: pojam brojeva do 20 (simbol, količina, imenovanje, slijed)</option>
                                        <option value="24">učenik je usvojio ili usvaja osnovne matematičke kompetencije: pojam brojeva do 100 (simbol, količina, imenovanje, slijed)</option>
                                        <option value="25">učenik je usvojio ili usvaja osnovne matematičke kompetencije: pojam brojeva više od 100 (simbol, količina, imenovanje, slijed)</option>
                                        <option value="31">učenik razumije računske radnje zbrajanja i oduzimanja: računa uz konkrete</option>
                                        <option value="32">učenik razumije računske radnje zbrajanja i oduzimanja: računa uz pomoć kalkulatora</option>
                                        <option value="4">učenik računa apstraktno (rješava jednostavne matematičke priče)</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentTime" sm={2}>Stupanj vještina snalaženja u vremenu (priroda i društvo)</Label>
                                <Col sm={5}>
                                    <select name="studentTime" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik nema razvijene vještine snalaženja u vremenu</option>
                                        <option value="1">učenik samostalno ili uz pomoć određuje i razumije doba dana (jutro, prijepodne, podne, poslijepodne, večer, noć)</option>
                                        <option value="2">učenik samostalno ili uz pomoć imenuje i razumije godišnja doba</option>
                                        <option value="3">učenik samostalno ili uz pomoć imenuje i razumije dane u tjednu</option>
                                        <option value="4">učenik samostalno ili uz pomoć određuje i razumije vremenske pojmove danas, jučer, sutra (prije/poslije)</option>
                                        <option value="5">učenik samostalno ili uz pomoć imenuje i razumije mjesece u godini</option>
                                        <option value="6">učenik samostalno ili uz pomoć očitava vrijeme na analognom satu (satu s kazaljkama)</option>
                                        <option value="7">učenik samostalno ili uz pomoć određuje i poznaje datum na kalendaru</option>
                                        <option value="8">učenik u potpunosti ima razvijene sve navedene vještine snalaženja u vremenu</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentMoney" sm={2}>Stupanj vještine upravljanja novcem</Label>
                                <Col sm={5}>
                                    <select name="studentMoney" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik nema razvijene vještine upravljanja novcem</option>
                                        <option value="1">učenik prepoznaje novac kao predmet</option>
                                        <option value="2">učenik razlikuje kovanice od novčanica</option>
                                        <option value="3">učenik određuje vrijednost novca pojedinačno</option>
                                        <option value="4">učenik razumije uporabnu vrijednost novca</option>
                                        <option value="5">učenik samostalno ili uz minimalnu pomoć obavlja kupovinu</option>
                                    </select>
                                </Col>
                            </FormGroup>
                            <h4 className="CategoryHeading">Socijalna domena</h4>
                            <FormGroup row>
                                <Label for="studentCommunication" sm={2}>Stupanj ekspresivne komunikacije</Label>
                                <Col sm={4}>
                                    <select name="studentCommunication" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">učenik ne komunicira smisleno (ni neverbalno ni verbalno)</option>
                                        <option value="1">učenik komunicira neverbalno putem mimike i geste ili putem potpomognutih oblika komunikacije (PECS, komunikatori…)</option>
                                        <option value="2">učenik komunicira verbalno oskudno s ili bez potpomognutih oblika komunikacije (PECS, komunikatori…)</option>
                                        <option value="3">učenik ima razvijenu optimalnu verbalnu komunikaciju</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentConversation" sm={2}>Stupanj konverzacije (sporazumijevanja)</Label>
                                <Col sm={4}>
                                    <select name="studentConversation" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">konverzacija s učenikom značajno otežana</option>
                                        <option value="1">konverzacija s učenikom moguća je uglavnom o temama iz njegove uobičajene socijalne situacije</option>
                                        <option value="2">konverzacija s učenikom je složenija i uključuje teme izvan njegove uobičajene socijalne situacije</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentLanguageUnderstanding" sm={2}>Stupanj jezičkog razumijevanja</Label>
                                <Col sm={3}>
                                    <select name="studentLanguageUnderstanding" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">izostanak ili vrlo ograničeno jezično razumijevanje</option>
                                        <option value="1">jezično razumijevanje gramatički i sadržajno jednostavnih rečenica/uputa</option>
                                        <option value="2">jezično razumijevanje gramatički i sadržajno složenijih rečenica/uputa</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentLanguageProduction" sm={2}>Stupanj jezične proizvodnje</Label>
                                <Col sm={3}>
                                    <select name="studentLanguageProduction" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">izostanak ili vrlo ograničena jezična proizvodnja</option>
                                        <option value="1">proizvodnja jednočlanih ili dvočlanih iskaza</option>
                                        <option value="2">proizvodnja jednostavnih iskaza (jednostavnih rečenica)</option>
                                        <option value="3">proizvodnja složenih rečeničnih iskaza</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentEmotions" sm={2}>Stupanj reguliranja emocija</Label>
                                <Col sm={3}>
                                    <select name="studentEmotions" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">često prisutne teškoće reguliranja emocija</option>
                                        <option value="1">osrednje prisutne teškoće u regulaciji emocija</option>
                                        <option value="2">rijetko prisutne teškoće u regulaciji emocija</option>
                                        <option value="3">nisu prisutne teškoće u regulaciji emocija</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentRules" sm={2}>Mogućnost slijeđenja pravila</Label>
                                <Col sm={3}>
                                    <select name="studentRules" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">često prisutne teškoće u slijeđenju dogovorenih pravila</option>
                                        <option value="1">osrednje prisutne teškoće u slijeđenju dogovorenih pravila</option>
                                        <option value="2">rijetko prisutne teškoće u slijeđenju dogovorenih pravila</option>
                                        <option value="3">nisu prisutne teškoće u slijeđenju dogovorenih pravila</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentChangingActivities" sm={2}>Mogućnost prelaska s aktivnosti na aktivnost</Label>
                                <Col sm={3}>
                                    <select name="studentChangingActivities" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">često prisutne teškoće u prelasku s aktivnosti na aktivnost</option>
                                        <option value="1">osrednje prisutne teškoće u prelasku s aktivnosti na aktivnost</option>
                                        <option value="2">rijetko prisutne teškoće u prelasku s aktivnosti na aktivnost</option>
                                        <option value="3">nisu prisutne teškoće u prelasku s aktivnosti na aktivnost</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentOtherPeople" sm={2}>Ponašanje u ovisnosti o drugim osobama u okruženju</Label>
                                <Col sm={3}>
                                    <select name="studentOtherPeople" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">veće promjene u ponašanju u ovisnosti o drugim osobama u okruženju</option>
                                        <option value="1">osrednje promjene u ponašanju u ovisnosti o drugim osobama u okruženju</option>
                                        <option value="2">manje promjene u ponašanju u ovisnosti o drugim osobama u okruženju</option>
                                        <option value="3">ne postoji promjena u ponašanju u ovisnosti o drugim osobama u okruženju</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentRiskSocSit" sm={2}>Razumijevanje rizičnih socijalnih situacija</Label>
                                <Col sm={3}>
                                    <select name="studentRiskSocSit" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">često prisutne teškoće u razumijevanju rizičnih socijalnih situacija</option>
                                        <option value="1">osrednje prisutne teškoće u razumijevanju rizičnih socijalnih situacija</option>
                                        <option value="2">rijetko prisutne teškoće u razumijevanju rizičnih socijalnih situacija</option>
                                        <option value="3">nisu prisutne teškoće u razumijevanju rizičnih socijalnih situacija</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentManipulation" sm={2}>Rizik za manipulaciju od drugih</Label>
                                <Col sm={3}>
                                    <select name="studentManipulation" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">često je sklon/a slijediti sugerirano ponašanje vršnjaka ne vodeći računa o svojoj dobrobiti</option>
                                        <option value="1">osrednje je sklon/a slijediti sugerirano ponašanje vršnjaka ne vodeći računa o svojoj dobrobiti</option>
                                        <option value="2">rijetko je sklon/a slijediti sugerirano ponašanje vršnjaka ne vodeći računa o svojoj dobrobiti</option>
                                        <option value="3">uopće nije sklon slijediti sugerirano ponašanje vršnjaka ne vodeći računa o svojoj dobrobiti</option>
                                    </select>
                                </Col>
                            </FormGroup>
                            <h4 className="CategoryHeading">Praktična domena - aktivnosti svakodnevnog života</h4>
                            <FormGroup row>
                                <Label for="studentFeeding" sm={2}>Podrška u hranjenju</Label>
                                <Col sm={3}>
                                    <select name="studentFeeding" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentDressing" sm={2}>Podrška u oblačenju</Label>
                                <Col sm={3}>
                                    <select name="studentDressing" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentHygiene" sm={2}>Podrška u obavljanju osobne higijene</Label>
                                <Col sm={3}>
                                    <select name="studentHygiene" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentSchool" sm={2}>Podrška pri odlasku u školu</Label>
                                <Col sm={3}>
                                    <select name="studentSchool" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentTransportation" sm={2}>Podrška prilikom korištenja sredstva javnog prijevoza</Label>
                                <Col sm={3}>
                                    <select name="studentTransportation" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentWorkAct" sm={2}>Podrška pri obavljanju svakodnevnih radnih aktivnosti kao npr. čišćenje, priprema jela, postavljanje stola...</Label>
                                <Col sm={3}>
                                    <select name="studentWorkAct" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0">u velikoj mjeri ili potpuno ovisan o podršci</option>
                                        <option value="1">osrednje ovisan o podršci</option>
                                        <option value="2">minimalno ovisan o podršci</option>
                                        <option value="3">osoba je samostalna i ne ovisi o podršci</option>
                                    </select>
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Label for="studentFreeTime" sm={2}>Podrška pri provođenju slobodnog vremena</Label>
                                <Col sm={3}>
                                    <select name="studentFreeTime" value={this.state.value} onChange={this.handleChange}>
                                        <option value="" disabled selected>Odaberite opciju</option>
                                        <option value="0"> izrazite teškoće u osmišljavanju aktivnosti slobodnog vremena</option>
                                        <option value="1">često potrebna podrška u osmišljavanju aktivnosti slobodnog vremena</option>
                                        <option value="2">osrednje potrebna podrška u osmišljavanju aktivnosti slobodnog vremena</option>
                                        <option value="3">rijetko potrebna ili nepotrebna podrška u osmišljavanju aktivnosti slobodnog vremena</option>
                                    </select>
                                </Col>
                            </FormGroup>

                    </Col>
                    <Col>
                        <FormGroup row>
                            <Col sm={5}>
                            </Col>
                            <Col sm={1}>
                                <button type="button" onClick={this.Addstudent} className="btn btn-success">Pohrani</button>
                            </Col>
                                <Col sm={1}>
                                    
                                    <NavLink to="/Studentlist"><Button color="danger">Odustani</Button>{' '}</NavLink>
                            </Col>
                            <Col sm={5}>
                            </Col>
                        </FormGroup>
                    </Col>
                    </Form>
                </div>
                <Footer></Footer>
            </div>





        );
    }

}

export default Addstudent;
