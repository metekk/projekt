﻿
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
class Table extends Component {
    constructor(props) {
        super(props);
    }

    DeleteStudent = () => {
        axios.delete('http://localhost:5000/Student/Deletestudent?id=' + this.props.obj.studentID)
            .then(res => {
                console.log(res);
                console.log(res.data);
                alert('Ucenik obrisan.');
                window.location.reload(false);
            })
    }



    render() {
        return (
            <tr>
                <td>
                    {this.props.obj.studentID}
                </td>
                <td>
                    {this.props.obj.initials}
                </td>
                <td>
                    {this.props.obj.age}
                </td>

                <td>
                    <Link to={"/Algoritmi/" + this.props.obj.studentID} className="btn btn-primary">Predloži igre</Link>
                </td>
                <td>
                    <Link to={"/edit/" + this.props.obj.studentID} className="btn btn-success">Ažuriraj podatke</Link>
                </td>
                <td>
                    <button type="button" onClick={this.DeleteStudent} className="btn btn-danger">Obriši učenika</button>
                </td>
            </tr>
        );
    }
}

export default Table;
