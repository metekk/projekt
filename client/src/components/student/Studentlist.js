﻿import React, { Component } from 'react';
import axios from 'axios';
import Table from './Table';
import { Navbar } from "../../components";
import { Footer } from "../../containers";
import { NavLink } from 'react-router-dom';
import './Studentlist.css';
import { getId } from '../../utils/common.js';


export default class Studentlist extends Component {

    constructor(props) {
        super(props);
        this.state = { business: [] };
    }
    componentDidMount() {
        axios.get("http://localhost:5000/Student/student-by-teacher/" + getId())
            .then(response => {
                this.setState({ business: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    tabRow() {
        return this.state.business.map(function (object, i) {
            return <Table obj={object} key={i} />;
        });
    }

    render() {
        return (
           
           <div className="App">
               <div className="gradient__bg">
                        <Navbar></Navbar>
                    <h4 align="center">Moji učenici</h4>
                    <div className="add-btn">
                        <NavLink to="/Addstudent"><button type="button">Dodaj novog učenika</button></NavLink>
                    </div>
                <table className="table table-striped" style={{ marginTop: 10 }}>
                    <thead>
                            <tr>
                            <th>Identifikacijski broj</th>
                            <th>Inicijali</th>
                            <th>Dob</th>
                            <th colSpan="5">Akcija</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.tabRow()}
                    </tbody>
                    </table>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}
