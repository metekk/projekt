import React from 'react';
import './cta.css';
import { NavLink } from 'react-router-dom';

const CTA = () => (
  <div className="games__cta">
    <div className="games__cta-content">
      <h3 className="gradient__text">Prikaži moje učenike.</h3>
    </div>
    <div className="games__cta-btn">
      <NavLink to="/studentlist"><button type="button">Moji učenici</button></NavLink>
    </div>
  </div>
);

export default CTA