﻿import React, {useState, useEffect} from "react";
import axios from "axios";
import { Link, useParams, NavLink } from "react-router-dom";
import { blog02 } from '../containers/blog/imports';
import {Navigate} from 'react-router-dom';
import back from '../assets/back.png';
import komentar from '../assets/komentar.svg';
import smile5 from '../assets/5.png';
import smile4 from '../assets/4.png';
import smile3 from '../assets/3.png';
import smile2 from '../assets/2.png';
import smile1 from '../assets/1.png';
import {IoMdArrowRoundBack} from 'react-icons/io';
import t from '../assets/3.svg';
import pet from '../assets/pet.png';
import './info.css';
import Comments from './Comments';
import StarRating from './StarRating.js';
import { Form, Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import ReactPaginate from 'react-paginate';

const Info = () => {

    const {id} = useParams();
    const [product, setProduct] = useState([]);
    const [loading, setLoading] = useState(false);
    const [redirect, setRedirect] = useState(false);

    
    const [reviews, setReviews] = useState([])
    const [pageNumber, setPageNumber] = useState(0);

    const usersPerPage = 300;
    const pagesVisited = pageNumber * usersPerPage;

    const pageCount = Math.ceil(reviews.length / usersPerPage);

    const changePage = ({ selected }) => {
        setPageNumber(selected);
    };
    useEffect(() => {
        const getProduct = async () => {
            setLoading(true);
            const response = await fetch(`http://localhost:5000/GamesInfoPage/games-info/${id}`);
            setProduct(await response.json());
            setLoading(false);
        }
        getProduct();
    }, []);

    const Loading = () => {
        return(
            <>
                Loading....
            </>
        )
    }


    useEffect(() => {
        const getComment = async () => {
            axios.get(`http://localhost:5000/Comment/games-comment/${id}`)
            .then(res => {
                console.log(res)
                setReviews(res.data)
            }).catch(err => {
                console.log(err)
            });
        };

    getComment();
    }, []);


    const url="http://localhost:5000/Comment/new-comment"
    const [data, setData] = useState({
        gameId : parseInt(id),
        teacherId : "",
        evaluation: "",
        comment: ""
    })

    function submit(e){
        e.preventDefault();
        axios.post(url,{
            gameId: data.gameId,
            teacherId: data.teacherId,
            evaluation: data.evaluation,
            comment: data.comment
        })
        .then(res =>{
            console.log(res.data)
            const getComment = async () => {
                axios.get(`http://localhost:5000/Comment/games-comment/${id}`)
                .then(res => {
                    console.log(res)
                    setReviews(res.data)
                }).catch(err => {
                    console.log(err)
                });
            };

            getComment();
            })
        //setRedirect(true);
    }

    //if(redirect){
      //  return <Navigate to="/igrice"></Navigate>
    //}


    function handle(e){
        const newdata ={...data}
        newdata[e.target.id] = e.target.value
        setData(newdata)
        console.log(newdata)
    }

    return(
        <div className="games__aboutus section__padding">
            <div className="info">
                <div className="back">
                    <NavLink to='/igrice'><IoMdArrowRoundBack></IoMdArrowRoundBack></NavLink>
                </div>
                
                <h1 className='gradient__text'>{product.gameName}</h1>
                <p>{product.description}</p>
                <p><a href={product.urLiOS}>Poveznica za preuzimanje igre za platformu iOS</a></p>
                <p><a href={product.urlAndroid}>Poveznica za preuzimanje igre za platformu Android</a></p>
                <p><a href={product.urLweb}>Poveznica za preuzimanje igre za web preglednik</a></p>
                <div className="games__aboutus-image">
                    <iframe src={product.gameVideoPath} frameborder="0" scrolling="no" allowfullscreen="true"></iframe>
                </div>
            </div>
            <img src={product.filePath}></img>
            
            
            <div className="klasa">
                <div className="klasa1">
                    <p>Komentari nastavnika</p>
                </div>
                <div className="scrollable-div" key={product.gameId}>
                    {reviews.length > 0 ? (
                        reviews.slice(pagesVisited, pagesVisited + usersPerPage).map(comments => {
                        return <div>
                            Šifra nastavnika: {comments.teacherId} <br/> Ocjena: {comments.evaluation} <br/> Komentar: {comments.comment} <hr/>
                            </div>
                    })
                    ) : (<p>Nema komentara</p>)}
                    </div>
                    <div className="klasa1">
                        <p>Dodajte svoj komentar</p>
                    </div>
                        <div className="card1">
                            <form onSubmit={(e)=>submit(e)}>
                                <label className="lab">Šifra nastavnika: </label>
                                <input onChange={(e)=>handle(e)} id="teacherId" value={data.teacherId} placeholder="Unesite svoju šifru" type="text"></input>
                                <label className="lab">Ocjena: </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                
                                <ul>
                                    <li>
                                        <input  onChange={(e)=>handle(e)} id="evaluation" value="1" name="ocjena" type="radio"></input><img src={smile1}></img> &nbsp;&nbsp;
                                    </li>
                                    <li>
                                        <input  onChange={(e)=>handle(e)} id="evaluation" value="2" name="ocjena" type="radio"></input><img src={smile2}></img> &nbsp;&nbsp;
                                    </li>
                                    <li>
                                        <input  onChange={(e)=>handle(e)} id="evaluation" value="3" name="ocjena" type="radio"></input><img src={smile3}></img> &nbsp;&nbsp;
                                    </li>
                                    <li>
                                        <input  onChange={(e)=>handle(e)} id="evaluation" value="4" name="ocjena" type="radio"></input><img src={smile4}></img> &nbsp;&nbsp;
                                    </li>
                                    <li>
                                        <input  onChange={(e)=>handle(e)} id="evaluation" value="5" name="ocjena" type="radio"></input><img src={smile5}></img> <br/>
                                    </li>
                                </ul>
                                <label className="lab">Komentar: </label>
                                <textarea onChange={(e)=>handle(e)} id="comment" value={data.comment} placeholder="komentar" ></textarea>
                                <div className="games__button">
                                    <button>Objavi</button>
                                </div>
                            </form>
                        </div> 
                    </div>
        </div>
    );
};

export default Info;