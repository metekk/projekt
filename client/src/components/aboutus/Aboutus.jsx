import React from 'react';
import aboutus from '../../assets/aboutus3.jpg';
import logo from '../../assets/DEI.png';
import './aboutus.css';
import { NavLink } from 'react-router-dom';

const Aboutus = () => (
  <div className="games__aboutus section__padding" id="possibility">
        <div className="games__aboutus-image">
            <div id="floated-imgs">
                <img src={logo}></img>
                <img src={aboutus}></img>
    </div>
   </div>
        
    <div className="games__aboutus-content">
      <h1 className="gradient__text">Svrha projekta</h1>
            <p>Svrha projekta je istraživanje mogućnosti korištenja digitalnih igara za unaprjeđenje kvalitete učenja i poučavanja
                korištenjem pristupa temeljenih na igri (eng. Game Based Learning, GBL). </p>
            <p> Detalje o projektu možete pronaći na  <a href="https://degames.uniri.hr/" target="_blank">službenim stranicama </a> Laboratorija za primjenu informacijskih tehnologija u obrazovanju (EduLab) pri Fakultetu informatike i digitalnih tehnologija, Sveučilište u Rijeci. </p>
      <h1 className="gradient__text">Učenje temeljeno na igri</h1>
            <p> Učenje temeljeno na igri (eng. Game-Based Learning – GBL) je pristup koji uključuje korištenje didaktičkih igara
                čija je svrha ostvarivanja određenih ishoda učenja. Danas se sve više koristi za motiviranje učenika, povećanje njihova angažmana
                te poboljšanje samih rezultata učenja. </p>
            <p>Igra je prirodna aktivnost djece i moguće ju je, osim za stjecanje znanja, koristiti i za razvoj sposobnosti i spremnosti učenika
                za rješavanje problema, donošenje odluka, metakogniciju, kritičko mišljenje, kreativnost i inovativnost. Također,
                korištenjem obrazovnih digitalnih igara kod učenika je moguće razvijati vještine komunikacije i suradnje te
                informacijsku i digitalnu pismenost već od nižih razreda osnovne škole.</p>

      <h2>U sklopu projekta istražit će se kako se koncepti GBL mogu iskoristiti za promicanje inkluzije učenika s intelektualnim teškoćama
                kroz pomoć u usvajanju novih podataka, razvoju novih vještina i stjecanju životnih kompetencija.</h2>

      <h1 className="gradient__text">Učenje temeljeno na igri za učenike s intelektualnim teškoćama</h1>
            <p>Jedan od glavnih razloga uvođenja digitalnih igra u odgoj i obrazovanje učenika s teškoćama u razvoju jest omogućiti im
                što lakše stjecanje funkcionalnih i adaptivnih vještina i znanja što za posljedicu ima lakšu integraciju i punopravnu
                participaciju u društvu čiji su dionici.</p>
            <p>Strategije aktivnog učenja i poučavanja podrazumijevaju učenje i poučavanje temeljeno na igri. Igra je sastavni dio učenja
                i poučavanja svih dobnih skupina, posebice učenika s teškoćama u razvoju. Poučavanje koje promatra igru kao kontekst koji
                uključuje poseban set ponašanja, u obzir uzima individualan pristup. Na takav se način određeni obrazovni sadržaji mogu
                približiti učenicima na prilagođen i njima razumljiv način. Upravo ta činjenica učenje temeljeno na igri čini interesantnim
                područjem za istraživanje. Učenje temeljeno na digitalnim igrama učenicima s intelektualnim teškoćama može pomoći u usvajanju
                novih podataka, usvajanju i razvoju novih vještina, stjecanju (životnih) kompetencija, razvoju socijalnih vještina i formiranju
                načina razmišljanja. Igra djeluje na učenika kroz bio, socijalni, kulturalni, emocionalni (afektivni), kognitivni i tjelesni
                aspekt te kao takva ima direktan utjecaj na ponašanje, način mišljenja i percepcije svijeta u kojem osoba živi i djeluje. </p>
     
        <div className='games__header-content__input'>
          
        </div>
    </div>
  </div>
);

export default Aboutus;