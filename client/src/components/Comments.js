import React, {useState, useEffect} from "react";
import Axios from "axios";

function Comments() {

    const url="http://localhost:5000/Comment/new-comment"
    const [data, setData] = useState({
        gameId: "",
        name : "",
        comment: ""
    })

    function submit(e){
        e.preventDefault();
        Axios.post(url,{
            gameId: parseInt(data.gameId),
            name: data.name,
            comment: data.comment
        })
        .then(res =>{
            console.log(res.data)
        })
    }

    function handle(e){
        const newdata ={...data}
        newdata[e.target.id] = e.target.value
        setData(newdata)
        console.log(newdata)
    }

    return(
        <div>
            <form onSubmit={(e)=>submit(e)}>
                <input onChange={(e)=>handle(e)} id="id" value={data.gameId} placeholder="id" type="number"></input>
                <input onChange={(e)=>handle(e)} id="name" value={data.name} placeholder="name" type="text"></input>
                <input onChange={(e)=>handle(e)} id="comment" value={data.comment} placeholder="comment" type="text"></input>
                <button>Submit</button>
            </form>
        </div>    
    );
};

export default Comments;