import React, { useState } from 'react';
import axios from 'axios';
import {Navigate, Link} from 'react-router-dom';
import { setUserSession } from '../../utils/common.js';

function LoginForm(props) {
  const [loading, setLoading] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const email = useFormInput('');
  const password = useFormInput('');
  const [error, setError] = useState(null);


  // handle button click of login form
  const handleLogin = () => {
    setError(null);
    setLoading(true);
    axios.post('http://localhost:5000/Auth/login', { email: email.value, password: password.value })
    .then(response => {
      setLoading(false);
      console.log(response.data)
      setUserSession(response.data.password, response.data.email, response.data.teacherId);
      setRedirect(true);
    }).catch(error => {
      setLoading(false);
      if (error.response) {
        // Request made and server responded
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
        setError("Pogrešan email ili lozinka."); 
      } else if (error.request) {
        // The request was made but no response was received
        console.log(error.request);
        setError("Pogrešan email ili lozinka."); 
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
        setError("Pogrešan email ili lozinka."); 
      }
    });
  }

  if(redirect){
    return <Navigate to="/home"></Navigate>
  }

  return (
    <div className="popup-box">
        <div className="box">
        <button className="btn-close" onClick={props.handleClose}>x</button>
            <main className="form-signin">
                <form>
                    <h2>Prijava za korištenje aplikacije</h2>
                    
                    <input type="email" className="form-control" id="floatingInput" placeholder="ime@email.com" {...email} autoComplete="new-password" />
                    
                    <input type="password" className="form-control" id="floatingPassword" placeholder="lozinka" {...password} autoComplete="new-password" />
                    <input type="button" className="w-100 btn btn-lg btn-primary" value={loading ? 'Učitavanje...' : 'Prijava'} onClick={handleLogin} disabled={loading} /><br />
                    <p>Niste registrirani?<Link className="linkprijava" to={`/registracija`}> Registrirajte se</Link></p>

                    {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}
                    
                </form>
            </main>
        </div>
    </div>
  );
};

const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}

export default LoginForm;