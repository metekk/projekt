import React, {useState} from "react";
import { RiMenu3Line, RiCloseLine } from 'react-icons/ri';
import {MdOutlineLogout} from 'react-icons/md';
import './navbar.css';
import DEI from '../../assets/DEI.png';
import {NavLink} from 'react-router-dom';
//BEM -> Block element modifier

const Navbar = () => {
  const [toggleMenu, setToggleMenu] = useState(false);
  const logout = async () => {
    await fetch("http://localhost:5000/Auth/logout", {
        method:'POST',
        headers: {'Content-Type': 'application/json'},
        credentials: 'include',
    });
  }
  
  return (
    <div className ="games__navbar">
      <div className="games__navbar-links">
        <div className="games__navbar-links_logo">
          <NavLink to="/home"><img src={DEI}></img>
            </NavLink>
        </div>
        <div className='games__navbar-links_container'>
          <p><NavLink to="/home" className="listItem" activeClassName="active-link">Početna</NavLink></p>
          <p><NavLink to="/Studentlist" className="listItem" activeClassName="active-link">Moji učenici</NavLink></p>
          <p><NavLink to="/igrice" className="listItem" activeClassName="active-link">Igre</NavLink></p>
          <p><NavLink to="/onama" className="listItem" activeClassName="active-link">O projektu</NavLink></p>
          <p><NavLink to="/kontakt" className="listItem" activeClassName="active-link">Kontakt</NavLink></p>
        </div>
      </div>

      <div className='games__navbar-sign'>
        <NavLink to="/"><button type='button' onClick={logout}>Odjava</button></NavLink>
        </div>
        <div className='games__navbar-menu'>
          {toggleMenu
            ?<RiCloseLine color = "#001e33" size={27} onClick={() => setToggleMenu(false)}/>
            :<RiMenu3Line color = "#001e33" size={27} onClick={() => setToggleMenu(true)}/>
          }
          {toggleMenu && (
            <div className='games__navbar-menu_container scale-up-center'>
              <div className='games__navbar-menu_container-links'>
               <p><NavLink to="/home" className="listItem" activeClassName="activeItem">Početna</NavLink></p>
               <p><NavLink to="/Studentlist" className="listItem" activeClassName="activeItem">Moji učenici</NavLink></p>
              <p><NavLink to="/igrice" className="listItem" activeClassName="activeItem">Igre</NavLink></p>
              <p><NavLink to="/onama" className="listItem" activeClassName="activeItem">O projektu</NavLink></p>
              <p><NavLink to="/kontakt" className="listItem" activeClassName="activeItem">Kontakt</NavLink></p>
              </div>
            </div>
          )}
          <div className="icone-out">
            <NavLink to="/"><MdOutlineLogout></MdOutlineLogout></NavLink>
          </div>
          </div>  
    </div>
  );
};

export default Navbar