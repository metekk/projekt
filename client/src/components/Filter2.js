import React, {useEffect} from "react";
import './filter.css';

const Filter2=({setActiveFilter2, activeFilter2, setFiltered2, post2})=>{

    useEffect(() =>{
        if(activeFilter2 === 0){
            setFiltered2(post2);
            return;
        }
        const filtered = post2.filter((res) => res.gamePlatform.includes(activeFilter2));
        setFiltered2(filtered);
    }, [activeFilter2]);

    return(
        <div className="filter-container">
            <select>
                <option onClick={() => setActiveFilter2(0)}>Sve</option>
                <option onClick={() => setActiveFilter2("Ios")}>Ios</option>
                <option onClick={() => setActiveFilter2("Android")}>Android</option>
            </select>
        </div>
    )
}

export default Filter2;