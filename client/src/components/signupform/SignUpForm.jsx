import React, { useState, SyntheticEvent } from 'react';
import './signupform.css';
import { Navigate, Link } from 'react-router-dom';

const SignUpForm = props => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [school, setSchool] = useState('');
    const [redirect, setRedirect] = useState(false);

    const Submit = async (e: SyntheticEvent) => {
        e.preventDefault();

        await fetch("http://localhost:5000/Auth/register", {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(
                {
                    name,
                    school,
                    email,
                    password
                })
        });

        setRedirect(true);
    }

    if (redirect) {
        return <Navigate to="/prijava"></Navigate>
    }

    return (
        <div className="popup-box">
            <div className="box">
                <button className="btn-close" onClick={props.handleClose}>x</button>
                <main className="form-signin">
                    <form onSubmit={Submit}>
                        <h2>Molim Vas registrirajte se za korištenje aplikacije</h2>
                        <input type="name" className="form-control" id="floatingInput" placeholder="Ime i prezime" required
                            onChange={e => setName(e.target.value)}
                        />
                        <input type="school" className="form-control" id="floatingInput" placeholder="Naziv institucije" required
                            onChange={e => setSchool(e.target.value)}
                        />
                        <input type="email" className="form-control" id="floatingInput" placeholder="ime@email.com" required
                            onChange={e => setEmail(e.target.value)}
                        />
                        <input type="password" className="form-control" id="floatingPassword" placeholder="lozinka" required
                            onChange={e => setPassword(e.target.value)}
                        />
                        <button className="w-100 btn btn-lg btn-primary" type="submit">Potvrdi podatke</button>
                        <p>Već imate korisnički račun? <Link className="linkprijava" to={`/prijava`}>Prijavite se</Link></p>
                    </form>
                </main>
            </div>
        </div>
    );
};

export default SignUpForm;
