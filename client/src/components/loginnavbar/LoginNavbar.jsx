import React, {useState} from 'react';
import { RiMenu3Line, RiCloseLine } from 'react-icons/ri';
import './loginnavbar.css';
import DEI from '../../assets/DEI.png';
import {NavLink} from 'react-router-dom';
import {SignUpForm} from '../';
import {LoginForm} from '../';
//BEM -> Block element modifier

const LoginNavbar = () => {
  const [toggleMenu, setToggleMenu] = useState(false);
  const[isOpen, setIsOpen] = useState(false);

  const togglePopup = () => {
    setIsOpen(!isOpen);
  }

  const[open, setOpen] = useState(false);

  const togglePopup2 = () => {
    setOpen(!open);
  }

  return (
    <div className ="lgames__navbar">
      <div className="games__navbar-links">
        <div className="games__navbar-links_logo">
        <NavLink to="/"><img src={DEI}></img></NavLink>
        </div>
      </div>

      <div className='games__navbar-sign'>
      <button type="button" onClick={togglePopup2}>Prijava</button>
      <button type='button' onClick={togglePopup}>Registracija</button>
        
        </div>
        <div className='games__navbar-menu'>
          {toggleMenu
            ?<RiCloseLine color = "#001e33" size={27} onClick={() => setToggleMenu(false)}/>
            :<RiMenu3Line color = "#001e33" size={27} onClick={() => setToggleMenu(true)}/>
          }
          {toggleMenu && (
            <div className='games__navbar-menu_container scale-up-center'>
              <div className='games__navbar-menu_container-links'>
                <div className='games__navbar-menu_container-links-sign'>
                <NavLink to="/prijava"><button type='button'>Prijava</button></NavLink>
                <NavLink to="/prijava"><button type='button'>Registracija</button></NavLink>
                </div>
              </div>
            </div>
          )}
          </div>  
          {isOpen && <SignUpForm
            handleClose={togglePopup}
          />}
          {open && <LoginForm
            handleClose={togglePopup2}
          />}
    </div>
  )
}

export default LoginNavbar