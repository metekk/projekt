import React from 'react';
import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import { RiMenu3Line, RiCloseLine } from 'react-icons/ri';
import {FaFilter} from 'react-icons/fa'
import {BiSquareRounded} from 'react-icons/bi'
import axios from 'axios';
import './datafetching.css';
import Button from 'react-bootstrap/Button';
import Filter from './Filter.js';
import Filters from './Filters.js';
import ReactPaginate from 'react-paginate';

function DataFetching() {
    const [post, setPost] = useState([]);
    const [loading, setLoading] = useState(false);
    // [filtered, setFiltered] = useState([]);
    const [filteredList, setFilteredList] = useState([]);
    // Selected Category name filter
    const [selectedCategory, setSelectedCategory] = useState("");
    // Selected Platform filter
    const [selectedPlatform, setSelectedPlatform] = useState("");

    const [selectedInstruction, setSelectedInstruction] = useState("");
    const [selectedRules, setSelectedRules] = useState("");
    const [selectedDuration, setSelectedDuration] = useState("");
    
    
    //const [activeFilter, setActiveFilter] = useState(0);
    const [pageNumber, setPageNumber] = useState(0);
    
    const [toggleMenu, setToggleMenu] = useState(false);

    const usersPerPage = 3;
    const pagesVisited = pageNumber * usersPerPage;

    const pageCount = Math.ceil(post.length / usersPerPage);

    const changePage = ({ selected }) => {
        setPageNumber(selected);
    };
    useEffect(() => {
        const fetchGames = async () => {
            setLoading(true);
            axios.get(`http://localhost:5000/GamesPage/games-page/`)
            .then(res => {
                console.log(res)
                setPost(res.data)
                setFilteredList(res.data)
            })
            .catch(err => {
                console.log(err)
            });
        };

    fetchGames();
    }, []);
    
    const filterByCategory = (filteredData) => {
        // Avoid filter for empty string
        if (!selectedCategory) {
          return filteredData;
        }
    
        const filteredGames = filteredData.filter(
          (post) => post.gameField.split(" ").indexOf(selectedCategory) !== -1
        );
        return filteredGames;
    };

      const filterByPlatform = (filteredData) => {
          // Avoid filter for null value
        if (!selectedPlatform) {
          return filteredData;
        }
    
        const filteredGames = filteredData.filter(
          (post) => post.gamePlatform.split(" ").indexOf(selectedPlatform) !== -1 
        );
        return filteredGames;
      };

      const filterByInstruction = (filteredData) => {
        // Avoid filter for empty string
        if (!selectedInstruction) {
          return filteredData;
        }
    
        const filteredGames = filteredData.filter(
          (post) => post.gameInstructions.split(" ").indexOf(selectedInstruction) !== -1
        );
        return filteredGames;
      };

      const filterByRules = (filteredData) => {
        // Avoid filter for empty string
        if (!selectedRules) {
          return filteredData;
        }
    
        const filteredGames = filteredData.filter(
          (post) => post.gameRules.split(" ").indexOf(selectedRules) !== -1
        );
        return filteredGames;
      };

      const filterByDuration = (filteredData) => {
        // Avoid filter for empty string
        if (!selectedDuration) {
          return filteredData;
        }
    
        const filteredGames = filteredData.filter(
          (post) => post.gameDuration.split(" ").indexOf(selectedDuration) !== -1
        );
        return filteredGames;
      };

      // Update seletedCategory state
    const handleCategoryChange = (event) => {
        setSelectedCategory(event.target.value);
    };

    // Toggle seletedPlatform state
    const handlePlatformChange = (event) => {
        setSelectedPlatform(event.target.value)
    };

    const handleInstructionChange = (event) => {
      setSelectedInstruction(event.target.value)
    };
    const handleRulesChange = (event) => {
      setSelectedRules(event.target.value)
    };
    const handleDurationChange = (event) => {
      setSelectedDuration(event.target.value)
    };

    useEffect(() => {
        var filteredData = filterByCategory(post);
        filteredData = filterByPlatform(filteredData);
        filteredData = filterByInstruction(filteredData);
        filteredData = filterByRules(filteredData);
        filteredData = filterByDuration(filteredData);
        setFilteredList(filteredData);
      }, [selectedCategory, selectedPlatform, selectedInstruction, selectedRules, selectedDuration]);

  return (
    <div>
      
        <h1 className='gradient__text'>Odaberite neku od igara na listi</h1>
        <br></br>
        <div className='games__filtering'>
        {toggleMenu
            ?<RiCloseLine color = "#001e33" size={25} onClick={() => setToggleMenu(false)}/>
            :<Button onClick={() => setToggleMenu(true)}>Filteri</Button>
        }

        {toggleMenu && (
          <div className="toggle">
            <div className="Category-filter">
            <p>Područje: </p>
              <select
                id="category-input"
                value={selectedCategory}
                onChange={handleCategoryChange}
              >
                <option value="" selected>Sve</option>
                <option value="finamotorika">Fina motorika</option>
                <option value="matematika">Matematika</option>
                <option value="aktivnostisvakodnevnogživota">Aktivnosti svakodnevnog života</option>
                <option value="komunikacija">Komunikacija</option>
                <option value="pisanje">Pisanje</option>
                <option value="vrijeme">Vrijeme</option>
                <option value="boje">Boje</option>
              </select>
          </div>
          <div className="platform-filter">
              <p>Platforma: </p>
              <select
                id="category-input"
                value={selectedPlatform}
                onChange={handlePlatformChange}
              >
                <option value="">Sve</option>
                <option value="Android">Android</option>
                <option value="iOS">iOS</option>
                <option value="Web">Web</option>
              </select>
              </div>
              <div className="platform-filter">
              <p>Upute: </p>
              <select
                id="category-input"
                value={selectedInstruction}
                onChange={handleInstructionChange}
              >
                <option value="">Sve</option>
                <option value="ne">Igra nema upute</option>
                <option value="da">Igra ima upute</option>
              </select>
              </div>
              <div className="platform-filter">
              <p>Pravila: </p>
              <select
                id="category-input"
                value={selectedRules}
                onChange={handleRulesChange}
              >
                <option value="">Sve</option>
                <option value="ne">Igra nema pravila</option>
                <option value="da">Igra ima pravila</option>
              </select>
              </div>
              <div className="platform-filter">
              <p>Trajanje: </p>
              <select
                id="category-input"
                value={selectedDuration}
                onChange={handleDurationChange}
              >
                <option value="">Sve</option>
                <option value="ne">Ograničeno trajanje aktivnosti</option>
                <option value="da">Slobodno igranje</option>
              </select>
              </div>
          </div>
          )}
        </div>
      
        <div className='item-container'>
            {filteredList.slice(pagesVisited, pagesVisited + usersPerPage).map((post) =>(
                <div className="card" key={post.gameID}>
                    <Link to={`/igrica/${post.gameID}`}><img src={post.filePath}></img></Link><br></br>
                    <Link to={`/igrica/${post.gameID}`}><h3>{post.gameName}</h3><br></br> </Link>
                    <p>{post.description}</p><br></br>
                <div className='link'>
                    <Link to={`/igrica/${post.gameID}`}>Više</Link>
                </div>
                </div>
            ))}
        </div>
            <ReactPaginate
            breakLabel="..."
            nextLabel=">>"
            onPageChange={changePage}
            pageRangeDisplayed={5}
            pageCount={pageCount}
            previousLabel="<"
            renderOnZeroPageCount={null}
            containerClassName={"paginationBttns"}
            previousLinkClassName={"previousBttn"}
            nextLinkClassName={"nextBttn"}
            disabledClassName={"paginationDisabled"}
            activeClassName={"paginationActive"}
        />
    </div>
  );
};

export default DataFetching