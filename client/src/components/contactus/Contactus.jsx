import React, { useRef, useState } from "react";
import emailjs from "emailjs-com";
import './contactus.css';
import contact from '../../assets/contact.svg';
import {Navigate} from 'react-router-dom';
// npm i @emailjs/browser

const Contactus = () => {
  const form = useRef();
  const [redirect, setRedirect] = useState(false);

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_sh07385",
        "template_9y1ytpq",
        form.current,
        "CiUKaKw8C4J5WEJOB"
      )
      .then(
        (result) => {
          console.log(result.text);
          console.log("message sent");
          window.location.reload();
        },
        (error) => {
          console.log(error.text);
        }
      );
  };

  return (
    <div class="container">
      <form ref={form} onSubmit={sendEmail}>
              <h1 className="gradient__text">Kontaktirajte nas</h1>
              <p> Za bilo kakve poteškoće ili pitanja u vezi rada sustava, slobodno ispunite formu. </p> 
        <label className="fname">Ime i prezime</label>
        <input type="text" id="fname" name="user_name" placeholder="Vaše ime i prezime..." />
        
        <label className="lname">Email</label>
        <input type="text" id="lname" name="user_email" placeholder="Vaš e-mail..." />
        
        <label className="subject">Poruka</label>
        <textarea id="subject" name="message" placeholder="Napišite nešto..."/>
        
        <input type="submit" value="Pošalji" />
      </form>
    </div>
    
  );
};

export default Contactus;
