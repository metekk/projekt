import React from 'react';

function CustomSearch({post}) {
    const[name, setName] = React.useState('');

    const handleSearch = () => {
        const newData = post.filter(x => x.post.gameField === name);
    }
    return(
        <div>
            <input type="text" placeholder="Enter..." onChange={(e) => setName(e.target.value)}></input>
            <button onClick={() => handleSearch}>Search</button>
        </div>
    )
}

export default CustomSearch;