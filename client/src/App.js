import React from "react";
import './App.css';
import { Route, Routes } from 'react-router-dom';
import Games from './pages/Games';
import Home from './pages/Home';
import Welcome from './pages/Welcome';
import About from './pages/About';
import Contact from './pages/Contact';
import SignUp from './pages/SignUp';
import Login from './pages/Login';
import ScrollToTop from "./pages/ScrollToTop";
import Game from "./pages/Game";
import Student from "./pages/Student";
import AddStudent from "./components/student/Addstudent"
import Studentlist from "./components/student/Studentlist"
import EditStudent from "./components/student/EditStudent"
import Algoritmi from "./components/student/Algoritmi"

import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all.js";
  
const App = () => {
    return (
        <>
            <ScrollToTop></ScrollToTop>
            
        <Routes>
            <Route exact path="/" element={<Welcome/>}></Route>
            <Route path="/home" element={<Home/>}></Route>
            <Route path="/igrice" element={<Games/>}></Route>
            <Route path='/igrica/:id' element={<Game/>} />
            <Route path="/onama" element={<About/>}></Route>
            <Route path="/kontakt" element={<Contact />}></Route>
            <Route path="/ucenici" element={<Student />}></Route> 
            <Route path="/registracija" element={<SignUp/>}></Route>
            <Route path="/prijava" element={<Login />}></Route>
            <Route path="/Addstudent" element={<AddStudent />}></Route>
            <Route path="/edit/:id" element={<EditStudent />}></Route>
            <Route path="/Studentlist" element={<Studentlist />}></Route>
            <Route path="/Algoritmi/:id" element={<Algoritmi />}></Route>

        </Routes>
            
        </>
    )
}
export default App