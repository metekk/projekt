import React from 'react';
import {Article} from '../../components';
import { blog02, blog03, blog04, blog05 } from './imports';
import './blog.css';

const Blog = () => (
  <div className="games__blog section__padding" id="blog">
    <div className="games__blog-heading">
      <h1 className="gradient__text">Odaberite neku od igrica s popisa, <br /> učite na zabavan način.</h1>
    </div>
    <div className="games__blog-container">
      <div className="games__blog-container_groupB">
        <Article imgUrl={blog02} date="Srpanj 20, 2022" text="Edukacijska igra 1" />
        <Article imgUrl={blog03} date="Srpanj 20, 2022" text="Edukacijska igra 2" />
        <Article imgUrl={blog04} date="Srpanj 20, 2022" text="Edukacijska igra 3" />
        <Article imgUrl={blog05} date="Srpanj 20, 2022" text="Edukacijska igra 4" />
      </div>
    </div>
  </div>
);

export default Blog;