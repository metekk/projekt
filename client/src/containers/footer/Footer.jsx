import React from 'react'
import './footer.css';
import { NavLink } from 'react-router-dom';
import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';

const Footer = () => {
  return (
    <div className="games__footer section__padding">

    <div className="games__footer-links">
      <div className="games__footer-links_div">
        <h4>Društvene mreže</h4>
        <div className="icons">
                      <a href="https://www.facebook.com/inf.uniri" target="_blank" ><FacebookIcon className="icon"></FacebookIcon></a>
                      <a href="https://hr.linkedin.com/in/kristianstancin" target="_blank" ><LinkedInIcon className="icon"></LinkedInIcon></a>
        </div>
      </div>
      <div className="games__footer-links_div">
        <h4>Navigacija</h4>
        <NavLink to="/ucenici"><p>Moji učenici</p></NavLink>
        <NavLink to="/onama"><p>O projektu</p></NavLink>
        <NavLink to="/kontakt"><p>Kontakt obrazac</p></NavLink>
      </div>
              <div className="games__footer-contact">
      <h4>Kontakt podaci</h4>
      <p>Sveučilište u Rijeci</p>
      <p>Fakultet informatike i digitalnih tehnologija</p>
      <p>Laboratorij za primjenu informacijskih tehnologija u obrazovanju - EduLab</p>
        <p>Radmile Matejčić 2, 51000 Rijeka</p>
        <p>(051)584-729</p>
        <p>kristian.stancin@inf.uniri.hr</p>
      </div>
    </div>

    <div className="games__footer-copyright">
              <p>@2022 DEGAMES - Digitalne igre u kontekstu učenja, poučavanja i promicanja inkluzivnog obrazovanja</p>
    </div>
  </div>
  );
}

export default Footer