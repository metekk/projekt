import React from 'react'
import { NavLink } from 'react-router-dom';
import { Feature } from '../../components';
import './games.css';

const Games = () => {
  return (
    <div className="games__whatgames section__margin" id="wgames">
      <div className="games__whatgames-feature">
              <p>SUSTAV ZA PREDLAGANJE DIGITALNIH OBRAZOVNIH IGARA KAO POTPORA PLANIRANJU ODGOJNO-OBRAZOVNIH AKTIVNOSTI UČENIKA S INTELEKTUALNIM TEŠKOĆAMA </p>
      </div>
    
    <div className="games__whatgames-container">
              <Feature title="CILJ" text="Cilj je sustava predlaganje digitalnih obrazovnih igara uzimajući u obzir
                                individualne odgojno-obrazovne potrebe učenika s intelektualnim teškoćama kao potporu
                                edukacijskim rehabilitatorima u unapređenju odgojno-obrazovnog procesa. " />
              <Feature title="SVRHA" text="Edukacijskim rehabilitatorima olakšati proces odabira prikladnih digitalnih obrazovnih igara za svoje učenike 
                                te ih na taj način dodatno potaknuti na korištenje suvremenih, inovativnih metoda poučavanja. " />
          </div>
          
  </div>
  )
}

export default Games