import React, {useState} from 'react'
import './welcomeheader.css';
import { LoginForm } from '../../components';
import DEI_puni_naziv from '../../assets/DEI_puni_naziv.png';
import { Feature } from '../../components';

 const WelcomeHeader = () => {

  const[open, setOpen] = useState(false);

  const togglePopup2 = () => {
    setOpen(!open);
  }
  return (
    <div>
    
        <div className='games__header-image'>
                  <img src={DEI_puni_naziv}></img>
        </div>
    
    {open && <LoginForm
            handleClose={togglePopup2}
          />}



          <div className="games__whatgames section__margin" id="wgames">
              <div className="games__whatgames-feature">
                  <p>Sustav za predlaganje digitalnih obrazovnih igara kao potpora planiranju odgojno-obrazovnih aktivnosti učenika s intelektualnim teškoćama </p>
              </div>

              <div className="games__whatgames-container">
                  <Feature title="CILJ" text="Cilj je sustava predlaganje digitalnih obrazovnih igara uzimajući u obzir
                                individualne odgojno-obrazovne potrebe učenika s intelektualnim teškoćama kao potporu
                                edukacijskim rehabilitatorima u unapređenju odgojno-obrazovnog procesa. " />
                  <Feature title="SVRHA" text="Edukacijskim rehabilitatorima olakšati proces odabira prikladnih digitalnih obrazovnih igara za svoje učenike 
                                te ih na taj način dodatno potaknuti na korištenje suvremenih, inovativnih metoda poučavanja. " />
              </div>

          </div>


      </div>
  )
}

export default WelcomeHeader